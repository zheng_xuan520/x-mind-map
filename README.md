## 🏆 GimiMind在线思维导图

### ✍️ 介绍
一款在线思维导图绘制的开源工具，支持多种结构/主题，支持主题高度自定义样式配置。

![xmind.jpeg](https://image.zdxblog.cn/picture/1739190382580-b47c9d14-c0a7-4053-9e41-d5eb43fff7b6.jpeg)

### 🛠 Node 版本

```js
>= 14
```

### 📚 项目运行

```shell
npm install

npm run serve
```

### 🔗 线上地址

[GimiMind在线思维导图](https://zdxblog.cn/xmind)

### 💯 交流

- [Issues](https://gitee.com/zheng_xuan520/x-mind-map/issues)