export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  const { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (direction === 'bottom') {
      if (tx !== sx) {
        return `M${sx} ${sy} L${sx} ${sy + 6 * multiple} ${tx} ${ty}`
      } else {
        return `M${sx} ${sy} ${tx - 1 * multiple} ${ty} ${sx + 1 * multiple} ${sy}`
      }
    }
    if (ty !== sy) {
      if (tx < sx) {
        return `M${sx} ${sy} L${sx - 6 * multiple} ${sy} ${tx} ${ty}`
      } else {
        return `M${sx} ${sy} L${sx + 6 * multiple} ${sy} ${tx} ${ty}`
      }
    } else {
      return `M${sx} ${sy - 1 * multiple} ${tx} ${ty} ${sx} ${sy + 1 * multiple}`
    }
  }
  return `M${sx} ${sy} L${tx} ${ty}`
}
