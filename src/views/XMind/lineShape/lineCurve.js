export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  const { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (direction === 'bottom') {
      if (Math.abs(tx - sx) < 10) {
        return `M${sx - 2 * multiple} ${sy + 2 * multiple} L${tx} ${ty} ${sx + 2 * multiple} ${sy + 8 * multiple}`
      }
      return `M${sx} ${sy + 2 * multiple} Q${tx} ${sy} ${tx} ${ty + 10 * multiple} Q${tx} ${sy} ${sx} ${sy + 8 * multiple}`
    }
    if (Math.abs(ty - sy) < 2) {
      return `M${sx} ${sy - 3 * multiple} L${tx} ${ty - 0.76 * multiple} ${tx} ${ty + 0.76 * multiple} ${sx} ${sy + 3 * multiple}`
    }
    const unitX = tx > sx ? 1 : -1
    const unitY = ty < sy ? 1 : -1
    return `M${sx} ${sy} Q${sx + (tx - sx) / 3} ${ty - 0.78 * multiple * unitY} ${tx} ${ty - 0.78 * multiple * unitY} L${tx} ${ty + 0.78 * multiple * unitY} Q${sx + (tx - sx) / 3 + 3 * multiple * unitX} ${ty + 0.78 * multiple * unitY} ${sx + 7 * multiple * unitX} ${sy}`
  }
  if (direction === 'bottom') {
    return `M${sx} ${sy} L${sx} ${sy + 6} Q${tx} ${sy + 6} ${tx} ${ty}`
  }
  if (tx < sx) {
    return `M${sx} ${sy} L${sx - 6} ${sy} Q${sx + (tx - sx) / 3} ${ty} ${tx} ${ty}`
  } else {
    return `M${sx} ${sy} L${sx + 6} ${sy} Q${sx + (tx - sx) / 3} ${ty} ${tx} ${ty}`
  }
}
