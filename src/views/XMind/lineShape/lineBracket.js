export default function ({ sourcePoint, targetPoint, gradient, lineWidth }) {
  const { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (ty === sy) {
      return `M${sx} ${sy - 2 * multiple} L ${tx} ${ty} ${sx} ${ty + 2 * multiple}`
    }
    const unitY = ty > sy ? 1 : -1
    return `M${sx} ${sy} Q${sx + 10 * multiple} ${sy} ${sx + 10 * multiple} ${sy + 10 * unitY * multiple} L${sx + 10 * multiple} ${ty - 15 * unitY * multiple} Q${sx + 10 * multiple} ${ty} ${sx + 25 * multiple} ${ty} Q${sx + 15 * multiple} ${ty} ${sx + 15 * multiple} ${ty - 10 * unitY * multiple} L${sx + 15 * multiple} ${sy + 15 * unitY * multiple} Q${sx + 15 * multiple} ${sy} ${sx} ${sy}`
  }
  if (ty === sy) {
    return `M${sx + 4} ${sy} L${tx - 4} ${ty}`
  }
  const radius = Math.min((tx - sx) / 4, 20)
  const sf = ty > sy ? 1 : 0
  const unitY = ty > sy ? -1 : 1
  return `M${sx + radius} ${sy} A${radius} ${radius} 0 0 ${sf} ${sx + radius * 2} ${sy + radius * (sf === 1 ? 1 : -1)} L${sx + radius * 2} ${ty + radius * unitY} A${radius} ${radius} 0 0 ${sf ? 0 : 1} ${sx + radius * 3} ${ty}`
}
