export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  let { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (direction === 'bottom') {
      sy += 8
      if (tx === sx) {
        return `M${sx - 2 * multiple} ${sy} L ${tx} ${ty} ${sx + 2 * multiple} ${sy}`
      }
      const unit = tx > sx ? 1 : -1
      return `M${sx} ${sy} L${tx + 2 * multiple * unit} ${sy} ${tx} ${ty} ${tx - 2 * multiple * unit} ${sy + 4 * multiple} ${sx} ${sy + 4 * multiple}`
    }
    const unit = tx > sx ? 1 : -1
    sx += 12 * unit
    if (ty === sy) {
      return `M${sx} ${sy - 2 * multiple} L${tx} ${ty - 0.75 * multiple} ${tx} ${ty + 0.75 * multiple} ${sx} ${ty + 2 * multiple}`
    }
    const unitY = ty > sy ? 1 : -1
    return `M${sx - 2 * multiple * unit} ${sy} L${sx - 2 * multiple * unit} ${ty + 2 * multiple * unitY} ${tx} ${ty + unitY * 0.75 * multiple} ${tx} ${ty - unitY * 0.75 * multiple} ${sx + 2 * multiple * unit} ${ty - 2 * multiple * unitY} ${sx + 2 * multiple * unit} ${sy}`
  }
  if (direction === 'bottom') {
    return `M${sx} ${sy} L${sx} ${sy + 12} L${tx} ${sy + 12} ${tx} ${ty}`
  }
  if (Math.abs(ty - sy) < 1) {
    return `M${sx} ${sy} L ${tx} ${ty}`
  } else {
    if (tx < sx) {
      return `M${sx} ${sy} L${sx - 12} ${sy} L${sx - 12} ${ty} ${tx} ${ty}`
    } else {
      return `M${sx} ${sy} L${sx + 12} ${sy} L${sx + 12} ${ty} ${tx} ${ty}`
    }
  }
}
