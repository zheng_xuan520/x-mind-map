export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  const { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  const diff = Math.abs(direction === 'bottom' ? sy - ty : sx - tx) / 2
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (direction === 'bottom') {
      if (tx < sx) {
        return `M${sx - 4 * multiple} ${sy} C${sx - 4 * multiple} ${sy + diff} ${tx} ${ty - diff} ${tx} ${ty} C${tx + 8 * multiple} ${ty - diff + 5 * multiple} ${sx} ${sy + diff} ${sx + 4 * multiple} ${sy}`
      } else if (tx > sx) {
        return `M${sx + 4 * multiple} ${sy} C${sx + 4 * multiple} ${sy + diff} ${tx} ${ty - diff} ${tx} ${ty} C${tx - 8 * multiple} ${ty - diff + 5 * multiple} ${sx} ${sy + diff} ${sx - 4 * multiple} ${sy}`
      } else {
        return `M${sx - 2 * multiple} ${sy} L ${tx} ${ty} ${sx + 2 * multiple} ${sy}`
      }
    }
    const d = Math.abs(ty - sy) / 18
    if (tx < sx) {
      if (ty < sy) {
        return `M${sx} ${sy - 2 * multiple} C${sx - diff} ${sy - d * multiple} ${tx + diff} ${ty - d * multiple / 5} ${tx} ${ty} C${tx + diff} ${ty + d * multiple} ${sx - diff} ${sy + 2 * multiple} ${sx} ${sy}`
      } else if (ty > sy) {
        return `M${sx} ${sy + 2 * multiple} C${sx - diff} ${sy + d * multiple} ${tx + diff} ${ty + d * multiple / 5} ${tx} ${ty} C${tx + diff} ${ty - d * multiple} ${sx - diff} ${sy - 2 * multiple} ${sx} ${sy}`
      } else {
        return `M${sx} ${sy - 2 * multiple} L ${tx} ${ty} ${sx} ${ty + 2 * multiple}`
      }
    } else {
      if (ty < sy) {
        return `M${sx} ${sy - 2 * multiple} C${sx + diff} ${sy - d * multiple} ${tx - diff} ${ty - d * multiple / 5} ${tx} ${ty} C${tx - diff} ${ty + d * multiple} ${sx + diff} ${sy + 2 * multiple} ${sx} ${sy}`
      } else if (ty > sy) {
        return `M${sx} ${sy + 2 * multiple} C${sx + diff} ${sy + d * multiple} ${tx - diff} ${ty + d * multiple / 5} ${tx} ${ty} C${tx - diff} ${ty - d * multiple} ${sx + diff} ${sy - 2 * multiple} ${sx} ${sy}`
      } else {
        return `M${sx} ${sy - 2 * multiple} L ${tx} ${ty} ${sx} ${ty + 2 * multiple}`
      }
    }
  }
  if (direction === 'bottom') {
    return `M${sx} ${sy} C${sx} ${sy + diff} ${tx} ${ty - diff} ${tx} ${ty}`
  }
  if (tx < sx) {
    return `M${sx} ${sy} C${sx - diff} ${sy} ${tx + diff} ${ty} ${tx} ${ty}`
  } else {
    return `M${sx} ${sy} C${sx + diff} ${sy} ${tx - diff} ${ty} ${tx} ${ty}`
  }
}
