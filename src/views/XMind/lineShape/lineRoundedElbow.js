export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  let { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 10
    if (direction === 'bottom') {
      sy += 8
      if (Math.abs(tx - sx) < 3) {
        return `M${sx - 2 * multiple} ${sy} L ${tx} ${ty} ${sx + 2 * multiple} ${sy}`
      }
      const unit = tx > sx ? 1 : -1
      return `M${sx} ${sy} L${tx - 8 * multiple * unit} ${sy} Q${tx + 2 * multiple * unit} ${sy} ${tx + 2 * multiple * unit} ${sy + 8 * multiple} L${tx} ${ty} ${tx - 2 * multiple * unit} ${sy + 10 * multiple} Q${tx - 2 * multiple * unit} ${sy + 4 * multiple} ${tx - 8 * multiple * unit} ${sy + 4 * multiple} L${sx} ${sy + 4 * multiple}`
    }
    const unitX = tx > sx ? 1 : -1
    sx += 12 * unitX
    if (Math.abs(ty - sy) < 1) {
      return `M${sx} ${sy - 2 * multiple} L ${tx} ${ty - 0.78 * multiple} ${tx} ${ty + 0.78 * multiple} ${sx} ${ty + 2 * multiple}`
    }
    const unitY = ty < sy ? 1 : -1
    return `M${sx - 2 * unitX * multiple} ${sy} L${sx - 2 * unitX * multiple} ${ty + 7.5 * multiple * unitY} Q${sx - 2 * unitX * multiple} ${ty - 2 * multiple * unitY} ${sx + 6 * multiple * unitX} ${ty - 2 * multiple * unitY} L${tx} ${ty - unitY * 0.78 * multiple} ${tx} ${ty + unitY * 0.78 * multiple} ${sx + 8 * multiple * unitX} ${ty + 1.5 * multiple * unitY} Q${sx + 2 * multiple * unitX} ${ty + 2 * multiple * unitY} ${sx + 2 * multiple * unitX} ${ty + 8 * multiple * unitY} L${sx + 2 * multiple * unitX} ${sy}`
  }
  const radius = 6
  if (direction === 'bottom') {
    const unit = tx < sx ? 1 : -1
    if (Math.abs(tx - sx) < 3) {
      return `M${sx} ${sy} L ${tx} ${ty}`
    }
    return `M${sx} ${sy} L${sx} ${sy + 12} ${tx + 5 * unit} ${sy + 12} A ${radius} ${radius} 0 0 ${unit > 0 ? 0 : 1} ${tx} ${sy + 17} L${tx} ${ty}`
  }
  const unit = tx > sx ? 1 : -1
  if (Math.abs(ty - sy) < 1) {
    return `M${sx} ${sy} L ${tx} ${ty}`
  } else if (ty < sy) {
    return `M${sx} ${sy} L${sx + 12 * unit} ${sy} ${sx + 12 * unit} ${ty + 5} A ${radius} ${radius} -90 0 ${unit > 0 ? 1 : 0} ${sx + 17 * unit} ${ty} L${tx} ${ty}`
  } else {
    return `M${sx} ${sy} L${sx + 12 * unit} ${sy} ${sx + 12 * unit} ${ty - 5} A ${radius} ${radius} -90 0 ${unit > 0 ? 0 : 1} ${sx + 17 * unit} ${ty} L${tx} ${ty}`
  }
}
