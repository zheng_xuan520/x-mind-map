export default function ({ sourcePoint, targetPoint, gradient, lineWidth, direction }) {
  const { sx, sy } = sourcePoint
  const { tx, ty } = targetPoint
  if (gradient) {
    const multiple = Number(lineWidth.split('-')[1]) / 2
    if (direction === 'bottom') {
      const unit = ty > sy ? 1 : -1
      const unitX = tx > sx ? 1 : -1
      const half = (ty - sy - 12 * unit) / 2
      if (Math.abs(tx - sx) < 1) {
        return `M${sx - multiple / 2} ${sy} L ${tx - multiple / 4} ${ty} ${tx + multiple / 4} ${ty} ${sx + multiple / 2} ${sy}`
      }
      return `M${sx - multiple / 2} ${sy} v${12 * unit} h${multiple} v${-12 * unit} M${sx} ${sy + 11 * unit} C${tx + unitX * multiple / 4} ${sy + 12 * unit + half} ${tx + unitX * multiple} ${sy + 12 * unit + half} ${tx + unitX * multiple / 4} ${ty} L${tx - unitX * multiple / 4} ${ty} C${tx - unitX * multiple / 4} ${sy + 12 * unit + half + unit * multiple / 4} ${tx - unitX * multiple / 4} ${sy + 12 * unit + half + unit * multiple / 4} ${sx} ${sy + 12 * unit + unit * multiple}`
    }
    const unit = tx > sx ? 1 : -1
    const unitY = ty > sy ? 1 : -1
    const half = (tx - sx - 12 * unit) / 2
    if (Math.abs(ty - sy) < 1) {
      return `M${sx} ${sy - multiple / 2} L ${tx} ${ty - multiple / 4} ${tx} ${ty + multiple / 4} ${sx} ${ty + multiple / 2}`
    }
    return `M${sx} ${sy - multiple / 2} h${12 * unit} v${multiple} h${-12 * unit} M${sx + 11 * unit} ${sy} C${sx + 12 * unit + half} ${ty + unitY * multiple / 4} ${sx + 12 * unit + half} ${ty + unitY * multiple} ${tx} ${ty + unitY * multiple / 4} L${tx} ${ty - unitY * multiple / 4} C${sx + 12 * unit + half + unit * multiple / 4} ${ty - unitY * multiple / 4} ${sx + 12 * unit + half + unit * multiple / 4} ${ty - unitY * multiple / 4} ${sx + 12 * unit + unit * multiple} ${sy}`
  }
  if (direction === 'bottom') {
    const unit = ty > sy ? 1 : -1
    const half = (ty - sy - 12 * unit) / 2
    if (Math.abs(tx - sx) < 1) {
      return `M${sx} ${sy} L ${tx} ${ty}`
    }
    return `M${sx} ${sy} L${sx} ${sy + 12 * unit} C${tx} ${sy + 12 * unit + half} ${tx} ${sy + 12 * unit + half} ${tx} ${ty}`
  }
  const unit = tx > sx ? 1 : -1
  const half = (tx - sx - 12 * unit) / 2
  if (Math.abs(ty - sy) < 1) {
    return `M${sx} ${sy} L ${tx} ${ty}`
  }
  return `M${sx} ${sy} L${sx + 12 * unit} ${sy} C${sx + 12 * unit + half} ${ty} ${sx + 12 * unit + half} ${ty} ${tx} ${ty}`
}
