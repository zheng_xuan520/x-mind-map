export default {
  'arrow-none': {
    viewBox: '0 0 0 0',
    refX: 0,
    refY: 0,
    markerWidth: 0,
    markerHeight: 0,
    d: ''
  },

  'arrow-end-triangle': {
    viewBox: '0 0 5 5',
    refX: 0,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M 0 2.5 L 0 0.5 4 2.5 0 4.5 z'
  },

  'arrow-end-dot': {
    viewBox: '0 0 5 5',
    refX: 0,
    refY: 2.2,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M 0 2.5 A 2 2 0 1 0 0 1.999 z'
  },

  'arrow-end-spearhead': {
    viewBox: '0 0 5 5',
    refX: 1.2,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M1.2 2.5 L0.2 0.2 4.8 2.5 0.2 4.8 1.2 2.5 z'
  },

  'arrow-end-square': {
    viewBox: '0 0 5 5',
    refX: 0.75,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M0.75 2.5 L0.75 0.75 4.25 0.75 4.25 4.25 0.75 4.25 z'
  },

  'arrow-end-doublearrow': {
    viewBox: '0 0 5 5',
    refX: 0.5,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M0 2.5 L0 1 2.5 2.5 0 4 M 2 2.5 L 2 0.5 5 2.5 2 4.5'
  },

  'arrow-end-antiTriangle': {
    viewBox: '0 0 5 5',
    refX: 1,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M0 2.5 L4.5 0.5 4.5 4.5'
  },

  'arrow-end-attached': {
    viewBox: '0 0 3 5',
    refX: 0,
    refY: 2.5,
    markerWidth: 5,
    markerHeight: 5,
    d: 'M0 2.5 L0 0.5 1 0.5 1 4.5 0 4.5'
  },

  'arrow-end-diamond': {
    viewBox: '0 0 5 5',
    refX: 0.5,
    refY: 2.5,
    markerWidth: 8,
    markerHeight: 5,
    d: 'M0 2.5 L2.5 0.5 5 2.5 2.5 4.5 z'
  }
}
