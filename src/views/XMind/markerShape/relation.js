export default {
  'arrow-none': {
    start: {
      viewBox: '0 0 0 0',
      refX: 0,
      refY: 0,
      markerWidth: 0,
      markerHeight: 0,
      d: ''
    },
    end: {
      viewBox: '0 0 0 0',
      refX: 0,
      refY: 0,
      markerWidth: 0,
      markerHeight: 0,
      d: ''
    }
  },

  'arrow-end-triangle': {
    start: {
      viewBox: '0 0 5 5',
      refX: 0,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M 0 2.5 L 5 0 5 5 0 2.5 z'
    },
    end: {
      viewBox: '0 0 5 5',
      refX: 4.5,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M 0 2.5 L 0 0 5 2.5 0 5 z'
    }
  },

  'arrow-end-dot': {
    start: {
      viewBox: '0 0 4 4',
      refX: 0,
      refY: 2,
      markerWidth: 4,
      markerHeight: 4,
      d: 'M 0 2 A 2 2 0 1 0 0 1.999 z'
    },
    end: {
      viewBox: '0 0 4 4',
      refX: 4,
      refY: 2,
      markerWidth: 4,
      markerHeight: 4,
      d: 'M 0 2 A 2 2 0 1 0 0 1.999 z'
    }
  },

  'arrow-end-spearhead': {
    start: {
      viewBox: '0 0 5 5',
      refX: 0.3,
      refY: 2.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M 3.8 2.5 L 4.8 4.8 L 0.2 2.5 L 4.8 0.2 L 3.8 2.5 z'
    },
    end: {
      viewBox: '0 0 5 5',
      refX: 4.5,
      refY: 2.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M1.2 2.5 L0.2 0.2 4.8 2.5 0.2 4.8 1.2 2.5 z'
    }
  },

  'arrow-end-square': {
    start: {
      viewBox: '0 0 5 5',
      refX: 1,
      refY: 2.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M0.75 2.5 L0.75 0.75 4.25 0.75 4.25 4.25 0.75 4.25 z'
    },
    end: {
      viewBox: '0 0 5 5',
      refX: 4,
      refY: 2.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M0.75 2.5 L0.75 0.75 4.25 0.75 4.25 4.25 0.75 4.25 z'
    }
  },

  'arrow-end-doublearrow': {
    start: {
      viewBox: '0 0 5 5',
      refX: 0,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L0 1 2.5 2.5 0 4 M 2 2.5 L 2 0.5 5 2.5 2 4.5'
    },
    end: {
      viewBox: '0 0 5 5',
      refX: 4.5,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L0 1 2.5 2.5 0 4 M 2 2.5 L 2 0.5 5 2.5 2 4.5'
    }
  },

  'arrow-end-antiTriangle': {
    start: {
      viewBox: '0 0 5 5',
      refX: 0,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L0 5 5 2.5 0 0'
    },
    end: {
      viewBox: '0 0 5 5',
      refX: 5,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L5 0 5 5'
    }
  },

  'arrow-end-attached': {
    start: {
      viewBox: '0 0 3 5',
      refX: 0,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L0 0.5 1 0.5 1 4.5 0 4.5'
    },
    end: {
      viewBox: '0 0 3 5',
      refX: 1,
      refY: 2.5,
      markerWidth: 5,
      markerHeight: 5,
      d: 'M0 2.5 L0 0.5 1 0.5 1 4.5 0 4.5'
    }
  },

  'arrow-end-diamond': {
    start: {
      viewBox: '0 0 4 3',
      refX: 0.2,
      refY: 1.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M0 1.5 L2 0 4 1.5 2 3 z'
    },
    end: {
      viewBox: '0 0 4 3',
      refX: 3.6,
      refY: 1.5,
      markerWidth: 8,
      markerHeight: 5,
      d: 'M0 1.5 L2 0 4 1.5 2 3 z'
    }
  }
}
