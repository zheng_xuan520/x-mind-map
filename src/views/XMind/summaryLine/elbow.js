export default {
  /**
   * 竖直方向上概要线条路径path
   * @param {*} start
   * @param {*} end
   * @param {*} direction
   * @returns
   */
  vertical: function (start, end, direction) {
    const { x: startx, y: starty } = start
    const { x: endx, y: endy } = end
    const center = `${(startx + endx) / 2} ${direction === 'bottom' ? starty + 12 : starty - 12}`
    return `M${startx} ${starty} L${center} ${endx} ${endy}`
  },

  /**
   * 水平方向上概要线条路径path
   * @param {*} start
   * @param {*} end
   * @param {*} direction
   * @returns
   */
  horizontal: function (start, end, direction) {
    const { x: startx, y: starty } = start
    const { x: endx, y: endy } = end
    const center = `${direction === 'right' ? startx + 10 : startx - 10} ${(starty + endy) / 2}`
    return `M${startx} ${starty} L${center} ${endx} ${endy}`
  }
}
