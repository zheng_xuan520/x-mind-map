import elbow from './elbow'
import square from './square'
import curve from './curve'
import straight from './straight'

export default {
  elbow,
  square,
  curve,
  straight
}
