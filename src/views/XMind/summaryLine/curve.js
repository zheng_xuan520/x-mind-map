export default {
  /**
   * 竖直方向上概要线条路径path
   * @param {*} start
   * @param {*} end
   * @param {*} direction
   * @returns
   */
  vertical: function (start, end, direction) {
    const { x: startx, y: starty } = start
    const { x: endx, y: endy } = end
    const center = `${(startx + endx) / 2} ${direction === 'bottom' ? starty + 10 : starty - 10}`
    const unit = direction === 'bottom' ? 1 : -1
    return `M${startx} ${starty - unit * 4} Q${center} ${endx} ${endy - unit * 4} M${(startx + endx) / 2} ${starty + unit * 4} v ${unit * 8}`
  },

  /**
   * 水平方向上概要线条路径path
   * @param {*} start
   * @param {*} end
   * @param {*} direction
   * @returns
   */
  horizontal: function (start, end, direction) {
    const { x: startx, y: starty } = start
    const { x: endx, y: endy } = end
    const center = `${direction === 'right' ? startx + 10 : startx - 10} ${(starty + endy) / 2}`
    const unit = direction === 'right' ? 1 : -1
    return `M${startx - unit * 4} ${starty} Q${center} ${endx - unit * 4} ${endy} M${startx + unit * 4} ${(starty + endy) / 2} h ${unit * 8}`
  }
}
