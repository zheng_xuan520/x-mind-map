import { select } from 'd3'
import { getTargetDataById, recursiveTreeValue } from '../utils/index'

export function handleInsertTask (root, currnentNode) {
  const isTask = currnentNode.data.isTask
  if (!isTask) {
    recursiveTreeValue(root, currnentNode.data._id, 'isTask', true)
    return true
  }
}

export function handleTaskCheckboxClick (event, _this, root) {
  event.stopPropagation()
  const id = select(_this.parentNode).datum().data._id
  const t = getTargetDataById(root, id)
  t.taskValue = Boolean(!t.taskValue)
  select(_this).select('rect')
    .attr('fill', t.taskValue ? '#620703' : 'transparent')
    .each(function () {
      select(this.parentNode).select('path').style('display', t.taskValue ? 'block' : 'none')
    })
}
