import { recursiveTreeValue, getTargetDataById } from '../utils'
import { select, selectAll } from 'd3-selection'
import { shortcutKeydown, resetcutKeydown } from '../shortcutKey'
let selectOutBorderId = null
/**
 * 向父级节点内插入外边框border数据
 * @param {*} root
 * @param {*} currnentNode
 * @returns
 */
export function handleInsertOutBorder (root, currnentNode) {
  const outBorder = currnentNode.data.outBorder
  const currentId = currnentNode.data._id
  if (!outBorder) {
    recursiveTreeValue(root, currentId, 'outBorder', { id: currentId, text: '' })
  } else {
    outBorderNodeHandleClick(`outborder-${currentId}`)
    return `outborder-${currentId}`
  }
}

/**
 * 外边框点击选中该外边框
 * @param {*} event
 * @param {*} _this
 */
export function outBorderNodeHandleClick (id, event, _this) {
  event && event.stopPropagation()
  removeSelectOutBorder()
  const targetElementNode = id ? select(`#${id}`) : select(_this)
  const subjectNodedata = targetElementNode.datum()
  const outBorderNode = targetElementNode.select('.outborder')
  const width = Number(outBorderNode.attr('width'))
  const height = Number(outBorderNode.attr('height'))
  const x = Number(outBorderNode.attr('x'))
  const y = Number(outBorderNode.attr('y'))
  select('.mind-map-outborder')
    .append('g')
    .attr('class', 'high-light-out-border')
    .append('rect')
    .attr('x', x - 4)
    .attr('y', y - 4)
    .attr('width', width + 8)
    .attr('height', height + 8)
    .attr('stroke-width', 1.8)
    .attr('rx', 4)
    .attr('ry', 4)
    .attr('stroke', '#69B0FF')
    .attr('fill', 'none')
    .attr('stroke-dasharray', function () {
      const len = select(this).node().getTotalLength()
      return `0 ${len}`
    })
    .transition()
    .duration(200)
    .attr('stroke-dasharray', function () {
      const len = select(this).node().getTotalLength()
      return `${len}, 0`
    })
  selectOutBorderId = targetElementNode.attr('id')
  if (!subjectNodedata.text) {
    select('.out-border-desc-text').style('display', 'block')
      .attr('transform', `translate(${x + 6},${y - 22})`)
      .select('.border-add-btn')
      .style('display', 'block')
      .each(function () {
        select(this.parentNode).select('foreignObject').style('display', 'none')
      })
  }
}

/**
 * 移除外边框选中样式
 */
export function removeSelectOutBorder () {
  selectAll('.high-light-out-border').remove()
  select('.out-border-desc-text').style('display', 'none')
  selectOutBorderId = null
}

export function deleteNodeOutBorder (root, callback) {
  if (!selectOutBorderId) return
  select(`#${selectOutBorderId}`).remove()
  const id = selectOutBorderId.replace('outborder-', '')
  const source = getTargetDataById(root, id)
  source.outBorder = null
  removeSelectOutBorder()
  callback && callback()
}

export function showOutBorderTextInput (event, _this, xmindTheme, root, callback) {
  event.stopPropagation()
  select(_this).style('display', 'none')
  const foreignObject = select(_this.parentNode).select('foreignObject').style('display', 'block')
  const outBorderSelection = select(`#${selectOutBorderId}`).select('.outborder')
  const width = Number(outBorderSelection.attr('width'))
  const height = foreignObject.select('div').text('').node().clientHeight
  foreignObject.attr('width', width - 12)
  foreignObject.attr('height', height)
  foreignObject.attr('y', -height + 20)
  foreignObject.select('div')
    .style('background-color', xmindTheme.outBorderColor)
    .on('focus', resetcutKeydown)
    .on('input', function () {
      const height = foreignObject.select('div').node().clientHeight
      foreignObject.attr('height', height)
      foreignObject.attr('y', -height + 20)
    })
    .on('blur', function (event) {
      shortcutKeydown()
      updateOutBorderText(root, event)
      callback && callback()
    })
    .node().focus()
}

export function updateOutBorderText (root, event) {
  const id = selectOutBorderId.replace('outborder-', '')
  const source = getTargetDataById(root, id)
  source.outBorder.text = event.target.innerText
}
