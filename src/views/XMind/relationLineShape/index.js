export default {
  angled: function (start, end, c1, c2) {
    return `M${start.x} ${start.y} L${c1.x} ${c1.y} ${c2.x} ${c2.y} ${end.x} ${end.y}`
  },

  curved: function (start, end, c1, c2) {
    return `M${start.x} ${start.y} C${c1.x} ${c1.y} ${c2.x} ${c2.y} ${end.x} ${end.y}`
  },

  straight: function (start, end) {
    return `M${start.x} ${start.y} L ${end.x} ${end.y}`
  },

  zigzag: function (start, end, c1, c2) {
    return `M${start.x} ${start.y} L${c1.x} ${c1.y} ${c2.x} ${c2.y} ${end.x} ${end.y}`
  }
}
