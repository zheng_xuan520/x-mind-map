export default {
  ArabiaNumber (num) {
    return `${num}. `
  },

  ArabiaParenthesisNumber (num) {
    return `(${num}) `
  },

  AlphaNumber (num, isLow) {
    const alphaChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    let alpha = ''

    let idx = Math.ceil(num / 26)

    while (idx > 0) {
      alpha += alphaChars[(num - 1) % 26]
      idx -= 1
    }

    if (isLow) {
      return `${alpha.toLocaleLowerCase()}. `
    }

    return `${alpha}. `
  },

  RomanNumber (num, isLow = false) {
    if (isNaN(num) || num <= 0 || num >= 4000) return ''

    const val = [
      1000, 900, 500, 400,
      100, 90, 50, 40,
      10, 9, 5, 4,
      1
    ]
    const sym = [
      'M', 'CM', 'D', 'CD',
      'C', 'XC', 'L', 'XL',
      'X', 'IX', 'V', 'IV',
      'I'
    ]
    let roman = ''
    let i = 0
    while (num > 0) {
      if (num >= val[i]) {
        roman += sym[i]
        num -= val[i]
      } else {
        i++
      }
    }
    if (isLow) {
      return `${roman.toLocaleLowerCase()}. `
    }
    return `${roman}. `
  },

  ChineseNumber (num) {
    const chineseNumbers = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
    const str = num.toString()
    let chineseStr = ''
    for (let i = 0; i < str.length; i++) {
      chineseStr += chineseNumbers[str[i]]
    }
    return `${chineseStr}、`
  }
}
