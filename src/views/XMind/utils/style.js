import { isEmpty } from './index'

/**
 * 根据节点所在层级设置节点样式（文字大小，背景色，文字颜色，边框，边框颜色...）
 * @param {*} nodes
 * @param { String } themeType 主题类型
 * @param { String } direction 子节点展开方向
 * @param { String } independent 子节点展开方向
 */
export default function (nodes, direction, skeletonTheme, independent) {
  const len = nodes.filter(n => n.depth === 1).length
  nodes.forEach((node, i) => {
    // 独立的主题均属于二级节点
    if (independent) {
      node.currentIdx = 0
    } else {
      if (node.depth === 0) (node.currentIdx = 0)
      if (node.depth === 1) (node.currentIdx = i - 1)
      if (node.depth > 1) (node.currentIdx = node.parent.currentIdx)
    }
    const key = node.depth > 1 ? 'normal' : node.depth + (independent ? 1 : 0) > 1 ? 'normal' : node.depth + (independent ? 1 : 0)
    node.style = JSON.parse(JSON.stringify(skeletonTheme[key]))
    const colors = node.style.colors
    const textColors = node.style.textColors
    const strokeColors = node.style.strokeColors
    if (node.depth > 0 || independent) {
      const nodeIdx = direction === 'left' ? len + node.currentIdx : node.currentIdx
      node.style.lineStyle.fill = strokeColors[nodeIdx % strokeColors.length]
      node.style.fill = colors[nodeIdx % colors.length]
      node.style.textStyle.color = textColors[nodeIdx % textColors.length]
      node.style.stroke = strokeColors[nodeIdx % strokeColors.length]
    } else {
      node.style.lineStyle.fill = strokeColors[0]
      node.style.fill = colors[0]
      node.style.textStyle.color = textColors[0]
      node.style.stroke = strokeColors[0]
    }
    const {
      fontFamily,
      fontSize,
      fontWeight,
      fontStyle,
      textDecoration,
      textDirection,
      maxWidth = 500,
      align,
      textColor,
      strokeColor,
      strokeStyle,
      strokeWidth,
      backgroundColor,
      branch,
      lineWidth,
      lineColor,
      horizontalOutter,
      lineEndJoin,
      shape,
      dot
    } = node.data.customStyle || {}
    if (!isEmpty(fontFamily)) {
      node.style.textStyle.fontFamily = fontFamily
    }
    if (!isEmpty(fontSize)) {
      node.style.textStyle.fontSize = fontSize
    }
    if (!isEmpty(fontWeight)) {
      node.style.textStyle.fontWeight = fontWeight
    }
    if (!isEmpty(maxWidth)) {
      node.style.textStyle.maxWidth = maxWidth
    }
    if (!isEmpty(fontStyle)) {
      node.style.textStyle.fontStyle = fontStyle
    }
    if (!isEmpty(textDecoration)) {
      node.style.textStyle.textDecoration = textDecoration
    }
    if (!isEmpty(textDirection)) {
      node.style.textStyle.textDirection = textDirection
    }
    if (!isEmpty(align)) {
      node.style.textStyle.align = align
    }
    if (!isEmpty(textColor)) {
      node.style.textStyle.color = textColor
    }
    if (!isEmpty(strokeColor)) {
      node.style.stroke = strokeColor
    }
    if (!isEmpty(strokeStyle)) {
      node.style.strokeStyle = strokeStyle
    }
    if (!isEmpty(strokeWidth)) {
      node.style.strokeWidth = strokeWidth
    }
    if (!isEmpty(backgroundColor)) {
      node.style.fill = backgroundColor
    }
    if (!isEmpty(dot)) {
      node.style.lineStyle.dot = dot
    }
    if (!isEmpty(branch)) {
      node.style.lineStyle.branch = branch
    }
    if (!isEmpty(lineWidth)) {
      node.style.lineStyle.lineWidth = lineWidth
    }
    if (!isEmpty(lineColor)) {
      node.style.lineStyle.fill = lineColor
      node.style.lineStyle.lineColor = lineColor
    }
    if (!isEmpty(horizontalOutter)) {
      node.style.spacing = horizontalOutter
    }
    if (!isEmpty(lineEndJoin)) {
      node.style.lineStyle.lineEndJoin = lineEndJoin
    }
    if (!isEmpty(shape)) {
      node.style.shape = shape
    }
  })
}
