import { exportGimiMind } from './index'

export async function downloadLocalFile (name, data) {
  if (window.showSaveFilePicker) {
    try {
      const fileHandle = await window.showSaveFilePicker({
        types: [
          {
            description: '',
            accept: { 'application/json': ['.gmind'] }
          }
        ],
        suggestedName: `${name}`
      })
      if (!fileHandle) return
      const string = JSON.stringify(data)
      const writable = await fileHandle.createWritable()
      await writable.write(string)
      await writable.close()
    } catch (error) {
    }
  } else {
    exportGimiMind(name, data)
  }
}
