import defaultSkeleton from './default'
import doubleTree from './doubleTree'

export default {
  defaultSkeleton: {
    defaultSkeleton,
    themeStyle: 'plain-fill-plain'
  },
  doubleTree: {
    doubleTree,
    themeStyle: 'fill-plain-plain-as-bgc'
  }
}
