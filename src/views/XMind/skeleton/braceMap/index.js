import defaultSkeleton from './default'
import polygonSide6Dashed from './polygonSide6Dashed'
import boldUnderline from './boldUnderline'
import doubleUnderline from './doubleUnderline'
import circleRect from './circleRect'
import defaultRough from './defaultRough'

export default {
  defaultSkeleton: {
    defaultSkeleton,
    themeStyle: 'plain-fill-plain'
  },
  polygonSide6Dashed: {
    polygonSide6Dashed,
    themeStyle: 'plain-plain-plain'
  },
  boldUnderline: {
    boldUnderline,
    themeStyle: 'plain-plain-plain'
  },
  doubleUnderline: {
    doubleUnderline,
    themeStyle: 'plain-plain-plain'
  },
  circleRect: {
    circleRect,
    themeStyle: 'plain-plain-plain'
  },
  defaultRough: {
    defaultRough,
    themeStyle: 'fill-fill-plain'
  }
}
