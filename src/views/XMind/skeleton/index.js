import mindMap from './mindMap'
import logicChart from './logicChart'
import orgChart from './orgChart'
import braceMap from './braceMap'
import treeMap from './treeMap'

export default {
  mindMap,
  logicChart,
  orgChart,
  braceMap,
  treeMap
}
