export default function (themeStyle) {
  const { backgroundColor, thumbColor, relationStyle, select, summaryStyle, outBorderColor, outBorderTextColor } = themeStyle
  const depth0Node = themeStyle['0']
  const depth1Node = themeStyle['1']
  const depth2Node = themeStyle.normal
  return {
    backgroundColor,
    thumbColor,
    relationStyle: {
      ...relationStyle,
      lineShape: 'zigzag',
      lineStyle: 'solid'
    },
    select,
    summaryStyle: {
      ...summaryStyle,
      lineShape: 'curve'
    },
    outBorderColor,
    outBorderTextColor,
    structure: 'org.xmind.ui.map.clockwise',
    0: {
      shape: 'default',
      colors: depth0Node.colors,
      textColors: depth0Node.textColors,
      strokeColors: depth0Node.strokeColors,
      textStyle: {
        fontSize: 26,
        fontWeight: 'bold',
        fontFamily: 'nevermind'
      },
      lineStyle: {
        lineWidth: 'a-10',
        branch: 'lineRoundedFold'
      },
      margin: {
        _t: 18,
        _r: 32,
        _b: 18,
        _l: 32
      },
      markSize: 20,
      linkSize: 19.5,
      tagSize: 12,
      spacing: 32,
      strokeWidth: 3,
      expandColor: depth0Node.expandColor,
      collapseColor: depth0Node.collapseColor
    },
    1: {
      shape: 'underline',
      colors: depth1Node.colors,
      textColors: depth1Node.textColors,
      strokeColors: depth1Node.strokeColors,
      textStyle: {
        fontSize: 18,
        fontWeight: 'normal',
        fontFamily: 'nevermind'
      },
      lineStyle: {
        lineWidth: 3,
        branch: 'lineBight'
      },
      margin: {
        _t: 10,
        _r: 20,
        _b: 10,
        _l: 20
      },
      markSize: 18,
      linkSize: 17.5,
      tagSize: 10,
      spacing: 82,
      strokeWidth: 3,
      strokeLineCap: 'buut',
      expandColor: depth1Node.expandColor,
      collapseColor: depth1Node.collapseColor
    },
    normal: {
      shape: 'underline',
      colors: depth2Node.colors,
      textColors: depth2Node.textColors,
      strokeColors: depth2Node.strokeColors,
      textStyle: {
        fontSize: 14,
        fontWeight: 'normal',
        fontFamily: 'nevermind'
      },
      lineStyle: {
        lineWidth: 3,
        branch: 'lineBight'
      },
      margin: {
        _t: 8,
        _r: 10,
        _b: 8,
        _l: 10
      },
      markSize: 15,
      linkSize: 15.5,
      tagSize: 10,
      spacing: 78,
      strokeWidth: 3,
      strokeLineCap: 'buut',
      expandColor: depth2Node.expandColor,
      collapseColor: depth2Node.collapseColor
    }
  }
}
