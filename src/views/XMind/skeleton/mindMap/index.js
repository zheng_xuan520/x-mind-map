import defaultSkeleton from './default'
import underlinePlain from './underlinePlain'
import circleRect from './circleRect'
import boldUnderline from './boldUnderline'
import thinUnderline from './thinUnderline'
import defaultRough from './defaultRough'
import ellipseRough from './ellipseRough'
import plainArrow from './plainArrow'
import singleBreakangleRough from './singleBreakangleRough'

export default {
  defaultSkeleton: {
    defaultSkeleton,
    themeStyle: 'fill-fill-fill'
  },
  underlinePlain: {
    underlinePlain,
    themeStyle: 'fill-plain-plain-as-bgc'
  },
  circleRect: {
    circleRect,
    themeStyle: 'fill-fill-plain'
  },
  boldUnderline: {
    boldUnderline,
    themeStyle: 'fill-plain-plain-as-bgc'
  },
  thinUnderline: {
    thinUnderline,
    themeStyle: 'fill-plain-plain'
  },
  plainArrow: {
    plainArrow,
    themeStyle: 'fill-plain-plain'
  },
  defaultRough: {
    defaultRough,
    themeStyle: 'plain-fill-plain'
  },
  ellipseRough: {
    ellipseRough,
    themeStyle: 'fill-fill-plain'
  },
  singleBreakangleRough: {
    singleBreakangleRough,
    themeStyle: 'fill-fill-plain'
  }
}
