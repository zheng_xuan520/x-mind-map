import defaultSkeleton from './default'
import fillText from './fillText'
import underlineText from './underlineText'
import defaultRough from './defaultRough'

export default {
  defaultSkeleton: {
    defaultSkeleton,
    themeStyle: 'fill-fill-plain'
  },
  fillText: {
    fillText,
    themeStyle: 'plain-plain-plain'
  },
  underlineText: {
    underlineText,
    themeStyle: 'plain-plain-plain'
  },
  defaultRough: {
    defaultRough,
    themeStyle: 'fill-fill-fill'
  }
}
