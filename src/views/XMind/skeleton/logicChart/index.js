import defaultSkeleton from './default'
import fillText from './fillText'
import fillUnderlineText from './fillUnderlineText'
import doubleQuotes from './doubleQuotes'
import doubleRect from './doubleRect'
import circleRectUnderlineRough from './circleRectUnderlineRough'
import defaultRough from './defaultRough'

export default {
  defaultSkeleton: {
    defaultSkeleton,
    themeStyle: 'fill-fill-fill'
  },
  fillText: {
    fillText,
    themeStyle: 'fill-plain-plain'
  },
  doubleQuotes: {
    doubleQuotes,
    themeStyle: 'plain-fill-plain'
  },
  fillUnderlineText: {
    fillUnderlineText,
    themeStyle: 'plain-plain-plain'
  },
  doubleRect: {
    doubleRect,
    themeStyle: 'plain-plain-plain'
  },
  circleRectUnderlineRough: {
    circleRectUnderlineRough,
    themeStyle: 'plain-fill-plain'
  },
  defaultRough: {
    defaultRough,
    themeStyle: 'fill-fill-fill'
  }
}
