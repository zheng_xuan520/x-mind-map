import { select } from 'd3-selection'
import mitter from '../mitt'

export function graphNodeComments (nodeContainer) {
  nodeContainer
    .selectAll('.x-mind-nodetheme')
    .select('.xmind-node-comment')
    .remove()

  const needRenderNodes = nodeContainer
    .selectAll('.x-mind-nodetheme')
    .filter(n => n.data.comment)

  needRenderNodes
    .append('g')
    .attr('class', 'xmind-node-comment')
    .attr('transform', d => {
      return `translate(0, ${-d.data.foreignObjectHeight / 2 + d.style.linkSize / 2})`
    })
    .on('click', function (event) {
      mitter.emit('comment-handler-click', { event, _this: this })
    })
    .append('svg')
    .attr('x', d => d.x + d.width - d.style.margin._r - d.style.linkSize)
    .attr('y', d => d.y + d.height - d.style.margin._t - d.style.linkSize - d.tagLineHeight - 1)
    .append('svg')
    .attr('width', d => d.style.linkSize)
    .attr('height', d => d.style.linkSize)
    .attr('fill', d => d.style.textStyle.color)
    .attr('viewBox', '0 0 1024 1024')
    .html(select('#icon-mb-menu-comment').node().innerHTML)
    .append('rect')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', 1024)
    .attr('height', 1024)
    .attr('fill', 'transparent')
}
