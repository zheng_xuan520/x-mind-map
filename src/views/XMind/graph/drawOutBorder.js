import { select } from 'd3-selection'
import { resetcutKeydown } from '../shortcutKey'
import { getXmindSummaryPos, getVerticalXmindSummaryPos, getOutBorderMinY } from '../utils/node'
import mitter from '../mitt'

let container = null

export function graphOutBorderContainer (mindContainer) {
  container = mindContainer
    .insert('g', '.mind-map-nodebox')
    .attr('class', 'mind-map-outborder')
  return container
}

export function graphOutborderElements (nodeContainer, skeletonTheme) {
  container.selectAll('g').remove()
  nodeContainer
    .selectAll('.x-mind-nodetheme')
    .filter(node => node.data.outBorder)
    .each(node => {
      node.direction === 'bottom' ? drawVerticalOutBorderNode(node, skeletonTheme) : drawHorizontalOutBorderNode(node, skeletonTheme)
    })
}

/**
 * 水平方向上的外边框绘制
 * @param {*} node
 */
function drawHorizontalOutBorderNode (node, skeletonTheme) {
  const outBorder = node.data.outBorder
  const dir = node.direction === 'right'
  const targetId = node.data._id
  const { minX, maxX, minY: mY, maxY } = getXmindSummaryPos(node, true)
  const minY = Math.min(getOutBorderMinY(node), mY)
  const rectStartX = minX + (dir ? -6 : 10)
  const rectStartY = minY - 6
  container
    .append('g')
    .attr('id', `outborder-${targetId}`)
    .datum(outBorder)
    .on('click', function (event) {
      mitter.emit('out-border-handler-click', { event, _this: this })
    })
    .append('rect')
    .attr('class', 'outborder')
    .attr('x', rectStartX)
    .attr('y', rectStartY)
    .attr('width', maxX - minX - 4)
    .attr('height', maxY - minY + 12)
    .attr('rx', 5)
    .attr('ry', 5)
    .attr('stroke', skeletonTheme.outBorderColor)
    .attr('stroke-dasharray', 5.5)
    .attr('stroke-width', 1)
    .attr('fill', `${skeletonTheme.outBorderColor}22`)
    .each(function () {
      select(this.parentNode).append('foreignObject')
        .attr('x', rectStartX + 6)
        .attr('y', rectStartY - outBorder.height - 1)
        .attr('width', maxX - minX - 4 - 12)
        .attr('height', outBorder.height)
        .append('xhtml:div')
        .attr('xmlns', 'http://www.w3.org/1999/xhtml')
        .attr('class', 'out-border-desc-input')
        .style('background-color', skeletonTheme.outBorderColor)
        .style('padding', '6px 14px')
        .style('border-radius', '6px 6px 1px 1px')
        .style('color', skeletonTheme.outBorderTextColor || '#fff')
        .style('font-size', '12px')
        .style('outline', 'none')
        .style('white-space', 'pre-wrap')
        .style('word-break', 'break-all')
        .style('display', 'inline-block')
        .style('font-family', 'nevermind')
        .attr('contenteditable', true)
        .on('mousedown', function (event) {
          event.stopPropagation()
          resetcutKeydown()
        })
        .on('input', function () {
          const height = select(this).node().clientHeight
          select(this.parentNode).attr('height', height).attr('y', rectStartY - height - 1)
        })
        .on('blur', function (event) {
          mitter.emit('update-out-border-text', { event })
        })
        .text(outBorder.text)
    })
}

/**
 * 竖直方向上的外边框绘制
 * @param {*} node
 */
function drawVerticalOutBorderNode (node, skeletonTheme) {
  const outBorder = node.data.outBorder
  const dir = node.direction === 'bottom'
  const targetId = node.data._id
  const { minX, maxX, minY, maxY } = getVerticalXmindSummaryPos(node)
  const rectStartX = minX - 6
  const rectStartY = minY + (dir ? -6 : 10)
  container
    .append('g')
    .attr('id', `outborder-${targetId}`)
    .datum(outBorder)
    .on('click', function (event) {
      mitter.emit('out-border-handler-click', { event, _this: this })
    })
    .append('rect')
    .attr('class', 'outborder')
    .attr('x', rectStartX)
    .attr('y', rectStartY)
    .attr('width', maxX - minX + 12)
    .attr('height', maxY - minY - 4)
    .attr('rx', 5)
    .attr('ry', 5)
    .attr('stroke', skeletonTheme.outBorderColor)
    .attr('stroke-dasharray', 5.5)
    .attr('stroke-width', 1)
    .attr('fill', `${skeletonTheme.outBorderColor}22`)
    .each(function () {
      select(this.parentNode).append('foreignObject')
        .attr('x', rectStartX + 6)
        .attr('y', rectStartY - outBorder.height - 1)
        .attr('width', maxX - minX)
        .attr('height', outBorder.height)
        .append('xhtml:div')
        .attr('xmlns', 'http://www.w3.org/1999/xhtml')
        .style('background-color', skeletonTheme.outBorderColor)
        .attr('class', 'out-border-desc-input')
        .style('padding', '6px 14px')
        .style('border-radius', '6px 6px 1px 1px')
        .style('color', skeletonTheme.outBorderTextColor || '#fff')
        .style('font-size', '12px')
        .style('outline', 'none')
        .style('white-space', 'pre-wrap')
        .style('word-break', 'break-word')
        .style('display', 'inline-all')
        .style('font-family', 'nevermind')
        .attr('contenteditable', true)
        .on('mousedown', function (event) {
          event.stopPropagation()
          resetcutKeydown()
        })
        .on('input', function () {
          const height = select(this).node().clientHeight
          select(this.parentNode).attr('height', height).attr('y', rectStartY - height - 1)
        })
        .on('blur', function (event) {
          mitter.emit('update-out-border-text', { event })
        })
        .text(outBorder.text)
    })
}
