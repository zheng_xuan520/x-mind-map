import { select } from 'd3-selection'
import mitter from '../mitt'
import { hiddenNodeHoverBorderPath } from './index'

/**
 * 绘制节点的展开收起按钮
 * @param {*} nodeContainer
 */
export function graphExpandNodeIcon (nodeContainer) {
  nodeContainer
    .selectAll('.child-ref-expand').remove()
  const expandNodesEnter = nodeContainer
    .selectAll('.x-mind-nodetheme')
    .filter(node => node.children?.length)
    .append('g')
    .attr('class', 'child-ref-expand')
    .on('click', function (event) {
      mitter.emit('expand-handler-click', { event, _this: this })
      hiddenNodeHoverBorderPath(select(this.parentNode))
    })

  expandNodesEnter
    .append('circle')
    .attr('class', 'expand-circle')
    .attr('cx', d => {
      return d.direction === 'bottom'
        ? d.x + d.width / 2
        : d.direction === 'left'
          ? d.x - 12 : d.x + d.width + 12
    })
    .attr('cy', d => {
      const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
      return d.direction !== 'bottom'
        ? d.y + spacing
        : d.y + d.height + 8
    })
    .attr('r', 4)
    .attr('fill', '#fff')
    .attr('stroke', d => d.style.lineStyle.fill)
    .attr('opacity', 0)

  expandNodesEnter
    .append('circle')
    .attr('class', 'expand-circle')
    .attr('cx', d => d.direction === 'left' ? d.x - 12 : d.direction === 'right' ? d.x + d.width + 12 : d.x + d.width / 2)
    .attr('cy',
      d => {
        const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
        return d.direction !== 'bottom'
          ? d.y + spacing
          : d.y + d.height + 8
      })
    .attr('r', 12)
    .attr('fill', 'transparent')

  expandNodesEnter
    .append('path')
    .attr('class', 'expand-path')
    .attr('d', d => {
      if (d.direction === 'left') {
        const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
        return `M${d.x - 14} ${d.y + spacing} L${d.x - 10} ${d.y + spacing}`
      }
      if (d.direction === 'right') {
        const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
        return `M${d.x + d.width + 10} ${d.y + spacing} L${d.x + d.width + 14} ${d.y + spacing}`
      }
      if (d.direction === 'bottom') {
        return `M${d.x + d.width / 2 - 2} ${d.y + d.height + 8} L${d.x + d.width / 2 + 2} ${d.y + d.height + 8}`
      }
    })
    .attr('stroke', d => d.style.collapseColor || d.style.lineStyle.fill)
    .attr('stroke-width', 2)
    .attr('opacity', 0)
}

/**
 * 收起后主题节点个数展示绘制
 * @param {*} nodeContainer
 */
export function graphNodeChildCount (nodeContainer) {
  const radius = 9
  nodeContainer.selectAll('.x-mind-nodetheme').select('.xmind-node-childcount').remove()

  const needRenderNodes = nodeContainer
    .selectAll('.x-mind-nodetheme')
    .filter(n => !n.data.expand && n.data.childCount)

  needRenderNodes
    .append('g')
    .attr('class', 'xmind-node-childcount')
    .on('click', function (event) {
      mitter.emit('expand-node-click', { event, _this: this })
      hiddenNodeHoverBorderPath(select(this.parentNode))
    })
    .append('path')
    .attr('d', d => {
      const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
      if (d.direction === 'left') {
        return `M${d.x}, ${d.y + spacing} L${d.x - radius}, ${d.y + spacing}`
      } else if (d.direction === 'right') {
        return `M${d.x + d.width}, ${d.y + spacing} L${d.x + d.width + radius}, ${d.y + spacing}`
      } else if (d.direction === 'bottom') {
        return `M${d.x + d.width / 2}, ${d.y + d.height} L${d.x + d.width / 2}, ${d.y + d.height + radius}`
      }
    })
    .attr('stroke-width', 2)
    .attr('stroke', d => d.style.lineStyle.fill)
    .each(function () {
      select(this.parentNode)
        .append('circle')
        .attr('cx', d => {
          return d.direction === 'left' ? d.x - radius * 2
            : d.direction === 'right' ? d.x + d.width + radius * 2 : d.x + d.width / 2
        })
        .attr('cy', d => {
          const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
          return d.direction === 'bottom' ? d.y + d.height + radius * 2 : d.y + spacing
        })
        .attr('r', radius)
        .attr('fill', d => d.style.lineStyle.fill)
      select(this.parentNode)
        .append('text')
        .attr('x', d => {
          return d.direction === 'left' ? d.x - radius * 2
            : d.direction === 'right' ? d.x + d.width + radius * 2 : d.x + d.width / 2
        })
        .attr('y', d => {
          const spacing = ['underline', 'doubleUnderline'].includes(d.style.shape) ? d.height : d.height / 2
          return (d.direction === 'bottom' ? d.y + d.height + radius * 2 : d.y + spacing) + 1
        })
        .text(d => d.data.childCount)
        .attr('font-size', d => d.data.childCount > 99 ? 8 : 10)
        .attr('font-weight', 'bold')
        .attr('text-anchor', 'middle')
        .attr('dominant-baseline', 'middle')
        .attr('fill', d => d.style.expandColor)
    })
}
