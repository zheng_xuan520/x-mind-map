import { select, selectAll } from 'd3-selection'
import { drag as d3Drag } from 'd3-drag'
import mitter from '../mitt'
import { getTopicNodeRect } from '../utils/size'

let svg = null
let container = null
let dragIng = false
let dragMovePosition = { x: 0, y: 0 }

export function graphLengendContainer (svgEl, mindContainer) {
  container = mindContainer
    .append('g')
    .attr('class', 'mind-map-lengend')
    .on('dblclick', event => event.stopPropagation())
  svg = svgEl
}

export function graphLengends (lengend, rootTopic) {
  container.select('foreignObject').remove()
  const { markers = [], position, visibility } = lengend || {}
  if (!visibility) return
  container.attr('transform', `translate(${position.x}, ${position.y})`)
  const existMarkers = getTopicMarkerIcon(rootTopic)
  const lendgends = existMarkers.map(icon => ({ icon, name: markers[icon] }))
  const { width, height } = getContainerSize(lendgends)
  container.append('foreignObject')
    .attr('width', width)
    .attr('height', height)
    .append('xhtml:div')
    .attr('xmlns', 'http://www.w3.org/1999/xhtml')
    .attr('class', 'lengend-box')
    .style('border-radius', '8px')
    .style('padding', '0 10px 8px')
    .style('border', '1px solid #e5dbdb')
    .style('background', 'rgba(255,255,255,0.5)')
    .append('p')
    .text('图例')
    .attr('style', 'text-align: center; color: #000; margin: 0; font-size: 14px; line-height: 38px; border-bottom: 1px solid #e5dbdb;')
    .call(
      d3Drag()
        .on('drag', function (event) {
          if ((event.dx !== 0 || event.dy !== 0) && !dragIng) {
            dragIng = true
            dragMovePosition = { x: event.x, y: event.y }
            container.style('opacity', 0.6)
            svg.classed('grabbing', true)
          }
          if (dragIng) {
            position.x -= (dragMovePosition.x - event.x)
            position.y -= (dragMovePosition.y - event.y)
            container.attr('transform', `translate(${position.x}, ${position.y})`)
          }
        })
        .on('end', function () {
          dragIng = false
          container.style('opacity', 1)
          svg.classed('grabbing', false)
          mitter.emit('set-lengend-position', position)
        }))
    .each(function () {
      select(this.parentNode).append('div')
        .style('margin-top', '8px')
        .html(generateLengendHtmlString(lendgends))
      selectAll('.lengend-editable-item').on('mousedown', event => event.stopPropagation())
        .on('blur', function () {
          const node = select(this).node()
          mitter.emit('set-lengend-name', {
            text: node.innerText,
            icon: node.getAttribute('data-marker')
          })
        })
    })
}

/**
 * 找出主题内所包含的marker icon集合并且去重
 * @param {*} root
 * @returns
 */
function getTopicMarkerIcon (root) {
  const cache = []
  function c (root, cache) {
    if (root.marks?.length) {
      root.marks.forEach(m => {
        cache.push(m.icon)
      })
    }
    if (root.children?.length) {
      root.children.forEach(child => {
        c(child, cache)
      })
    }
  }
  c(root, cache)

  return [...new Set(cache)]
}

function generateLengendHtmlString (lendgends) {
  if (!lendgends.length) {
    return `<div style="font-size: 0; padding-top: 10px">
      <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTM2IiBoZWlnaHQ9IjQyIiB2aWV3Qm94PSIwIDAgMTM2IDQyIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+PHBhdGggZD0iTTEzNS4yMzcgNDJoLTcuNDM5Yy0yLjQ2LTguMDI0LTExLjkxNC0xNC0yMy4yMDItMTQtMy42MDYgMC03LjAyNC42MS0xMC4wODcgMS43MDFNNDIuNjUgMzEuMTExQzM4LjkxNSAyOS4xNDcgMzQuNDEyIDI4IDI5LjU2NSAyOGMtMTEuMDYyIDAtMjAuMzMgNS45NzMtMjIuNzY3IDE0SDAiIHN0cm9rZT0iI0UxRTNFNSIgb3BhY2l0eT0iLjgiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIvPjxwYXRoIGQ9Ik0xMDAuNSAzNi42OTJDOTQuODkgMjcuOTk3IDgzLjAwOCAyMiA2OS4yNTcgMjIgNTIuODU1IDIyIDM5LjExMiAzMC41MzMgMzUuNSA0MiIgc3Ryb2tlPSIjQURCOUI5IiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48cGF0aCBkPSJNNjcuNzk4IDEuNzA4VjIxLjVhLjUuNSAwIDEgMC0xIDBWLjVhLjUuNSAwIDAgMCAxIDB2MS4yMDhDNjkuNzkxLjU3IDcxLjkwNSAwIDc0LjEzOCAwYzMuMzUgMCA2LjUxIDEuMDQ4IDguMjczIDEuMDQ4IDEuMTc2IDAgMi4zODQtLjE3OSAzLjYyNi0uNTM2VjEyLjUyYy0xLjIyMi4zNDMtMi40My41MTQtMy42MjYuNTE0LTEuNzkzIDAtNS4wNTEtMS4wMjYtOC4yNzMtMS4wMjYtMi4xNDggMC00LjI2MS41NjItNi4zNCAxLjY4NlYyMS41YS41LjUgMCAxIDEtMSAwVi41YS41LjUgMCAwIDEgMSAwdjEuMjA4WiIgZmlsbD0iI0FEQjlCOSIgZmlsbC1ydWxlPSJub256ZXJvIi8+PGcgdHJhbnNmb3JtPSJyb3RhdGUoLTI3IDMwLjQ5NiAtMzAuMjM2KSI+PHBhdGggZD0iTTUuNzI2IDE1Ljk3NGMzLjMzNC00LjU2IDUtNy44MDkgNS05Ljc0NCAwLTIuOTAzLTIuMjM4LTUuMjU2LTUtNS4yNTYtMi43NiAwLTUgMi4zNTMtNSA1LjI1NiAwIDEuOTM1IDEuNjY3IDUuMTgzIDUgOS43NDRaIiBzdHJva2U9IiNFMUUzRTUiIG9wYWNpdHk9Ii44Ii8+PGNpcmNsZSBmaWxsPSIjRTFFM0U1IiBvcGFjaXR5PSIuODAxIiBjeD0iNS45NyIgY3k9IjUuNzkzIiByPSIyIi8+PC9nPjxnIHRyYW5zZm9ybT0icm90YXRlKDIwIDMwLjY0NCAzMDYuOTA3KSIgb3BhY2l0eT0iLjc5NyI+PGNpcmNsZSBzdHJva2U9IiNFMUUzRTUiIGN4PSI3IiBjeT0iNyIgcj0iNyIvPjxjaXJjbGUgZmlsbD0iI0UxRTNFNSIgY3g9IjQuNjY3IiBjeT0iNi4yMjIiIHI9IjEiLz48Y2lyY2xlIGZpbGw9IiNFMUUzRTUiIGN4PSI5LjMzMyIgY3k9IjYuMjIyIiByPSIxIi8+PHBhdGggZD0iTTMuMTExIDkuNDgyYzEuMDM3IDEuNTIgMi4zMzMgMi4yODEgMy44ODkgMi4yODEgMS41NTYgMCAyLjg1Mi0uNzYgMy44ODktMi4yODFsLS43NzguMTFDOC44MzYgOS4yNTYgNy44IDkuMDg4IDcgOS4wODhjLS43OTkgMC0xLjgzNi4xNjgtMy4xMTEuNTA0bC0uNzc4LS4xMVpNMTEuODA0IDUuMjNjLS4zNjUtLjU0Ni0uNzk2LS45My0xLjI5MS0xLjE1LS43NDQtLjMzMy0xLjYyMi0uMTgtMS44NTguMDc0LS4xMjMuMTMzLS4yMDguNTUuMjI0LjU3LjM5LjAxOC44NTItLjE2NSAxLjQ0NS0uMTI2LjQ4NC4wMzEuOTc3LjI0MiAxLjQ4LjYzMVpNMi4zMzMgNS4yNThjLjM2Ni0uNTQ1Ljc5Ni0uOTI5IDEuMjkyLTEuMTUuNzQzLS4zMzIgMS42MjEtLjE4IDEuODU3LjA3NC4xMjQuMTM0LjIwOC41NTEtLjIyMy41Ny0uMzkxLjAxOS0uODUyLS4xNjUtMS40NDUtLjEyNi0uNDg0LjAzMi0uOTc4LjI0Mi0xLjQ4LjYzMloiIGZpbGw9IiNFMUUzRTUiLz48L2c+PC9nPjwvc3ZnPg==" alt="" />
      <p style="line-height: 24px; font-size: 13px; color: #adb9b9; text-align: center; margin-top: 10px">将图标插入至主题中</p>
    </div>`
  }
  return lendgends.reduce((prev, cur) => {
    prev += `<div style="display: flex; align-items: center; height: 26px; font-size: 0;">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0,0,1024,1024" width="16" height="16" style="background: #fff; border-radius: 50%; flex-shrink: 0">
        ${select(`#${cur.icon}`).node().innerHTML}
      </svg>
      <div class="lengend-editable-item" contenteditable="true" data-marker="${cur.icon}" style="margin-left: 8px; outline: none; font-size: 14px; font-family: nevermind; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; flex: 1; width: 0;">${cur.name}</div>
    </div>`

    return prev
  }, '')
}

function getContainerSize (lendgends) {
  const height = 38 + 8 + 8 + (lendgends.length ? lendgends.length * 26 : 86) + 8 + 4
  const markerWidths = lendgends.map(o => {
    return getTopicNodeRect({
      fontFamily: 'nevermind',
      fontSize: 14,
      maxWidth: '230px',
      text: o.name
    }).width
  })
  const width = Math.max(Math.max(...markerWidths) + 8 + 20 + 16 + 4, 156)

  return { width, height }
}
