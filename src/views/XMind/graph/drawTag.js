/**
 * 主题节点上标签绘制
 * @param {*} nodeContainer
 */
const tagColors = ['#FFBC9F', '#006D77', '#D8AC8F', '#83C5BE']
export function graphNodeTags (nodeContainer) {
  nodeContainer
    .selectAll('.xmind-node-tags')
    .remove()
  const tagNodesEnter = nodeContainer
    .selectAll('.x-mind-nodetheme')
    .filter(node => node.data.tag)
    .append('g')
    .attr('class', 'xmind-node-tags')
    .attr('transform', d => {
      return `translate(${d.style.margin._l}, ${d.height - d.style.margin._b})`
    })
    .selectAll('g')
    .data(d => d.data.tagsInfo.map(o => {
      return {
        ...o,
        _id: d.data._id,
        x: d.x,
        y: d.y,
        width: d.width,
        height: d.height,
        style: d.style,
        comment: d.data.comment,
        link: d.data.link,
        foreignObjectWidth: d.data.foreignObjectWidth
      }
    }))
    .enter()
    .append('g')
  tagNodesEnter
    .append('text')
    .text(d => d.tagName)
    .attr('font-size', d => d.style.tagSize)
    .attr('fill', '#fff')
    .attr('x', (d, i) => {
      return d.x + d.prevWidth + i * 8 + 6
    })
    .attr('y', d => d.y - 3)
    .attr('dominant-baseline', 'ideographic')

  tagNodesEnter
    .insert('rect', 'text')
    .attr('x', (d, i) => {
      return d.x + d.prevWidth + i * 8
    })
    .attr('y', d => d.y - d.tagHeight)
    .attr('rx', 3)
    .attr('ry', 3)
    .attr('width', d => d.tagWidth)
    .attr('height', d => d.tagHeight)
    .attr('fill', (d, i) => tagColors[i % 4])
}
