export default [
  {
    name: '无编号',
    operate: 'insert-serial',
    key: undefined
  },
  {
    name: '1.，2.，3.',
    operate: 'insert-serial',
    key: 'ArabiaNumber'
  },
  {
    name: '(1)，(2)，(3)',
    operate: 'insert-serial',
    key: 'ArabiaParenthesisNumber'
  },
  {
    name: 'a.，b.，c.',
    operate: 'insert-serial',
    key: 'AlphaNumber-low'
  },
  {
    name: 'A.，B.，C.',
    operate: 'insert-serial',
    key: 'AlphaNumber'
  },
  {
    name: 'i.，ii.，iii.',
    operate: 'insert-serial',
    key: 'RomanNumber-low'
  },
  {
    name: 'I.，II.，III.',
    operate: 'insert-serial',
    key: 'RomanNumber'
  },
  {
    name: '一.，二.，三.',
    operate: 'insert-serial',
    key: 'ChineseNumber'
  }
]
