export default [
  {
    name: '思维导图',
    skeleton: 'mindMap',
    dataList: [
      {
        name: 'defaultSkeleton'
      },
      {
        name: 'underlinePlain'
      },
      {
        name: 'circleRect'
      },
      {
        name: 'boldUnderline'
      },
      {
        name: 'thinUnderline'
      },
      {
        name: 'plainArrow'
      },
      {
        name: 'defaultRough',
        isRough: true
      },
      {
        name: 'ellipseRough',
        isRough: true
      },
      {
        name: 'singleBreakangleRough',
        isRough: true
      }
    ]
  },
  {
    name: '逻辑结构图',
    skeleton: 'logicChart',
    dataList: [
      {
        name: 'defaultSkeleton'
      },
      {
        name: 'fillText'
      },
      {
        name: 'fillUnderlineText'
      },
      {
        name: 'doubleRect'
      },
      {
        name: 'doubleQuotes'
      },
      {
        name: 'circleRectUnderlineRough',
        isRough: true
      },
      {
        name: 'defaultRough',
        isRough: true
      }
    ]
  },
  {
    name: '组织架构图',
    skeleton: 'orgChart',
    dataList: [
      {
        name: 'defaultSkeleton'
      },
      {
        name: 'fillText'
      },
      {
        name: 'underlineText'
      },
      {
        name: 'defaultRough',
        isRough: true
      }
    ]
  },
  {
    name: '括号图',
    skeleton: 'braceMap',
    dataList: [
      {
        name: 'defaultSkeleton'
      },
      {
        name: 'polygonSide6Dashed'
      },
      {
        name: 'boldUnderline'
      },
      {
        name: 'doubleUnderline'
      },
      {
        name: 'circleRect'
      },
      {
        name: 'defaultRough',
        isRough: true
      }
    ]
  },
  {
    name: '树形图',
    skeleton: 'treeMap',
    dataList: [
      {
        name: 'defaultSkeleton'
      },
      {
        name: 'doubleTree'
      }
    ]
  }
]
