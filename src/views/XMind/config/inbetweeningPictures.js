export default [
  {
    name: '生产力',
    list: [
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/calendar_planning.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/computer.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/presentation.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/briefcase.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/finance.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/inspiration.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/painting.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Productivity/sketch.svg?v=0.3.7'
    ]
  },
  {
    name: '旅行',
    list: [
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/beach.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/forest.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/space.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/travel.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/steamship.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/plane.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/taxi.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/bus.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/hotair_ballon.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Travel/helicopter.svg?v=0.3.7'
    ]
  },
  {
    name: '假期',
    list: [
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/happy_birthday.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/moon.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/halloween.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/christmas.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/spring_festival.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/confetti.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/magic.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/movies.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Holiday/open_air.svg?v=0.3.7'
    ]
  },
  {
    name: '食品与饮料',
    list: [
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/fastfood.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/meat.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/dim_sum.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/vegetable.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/fruit.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/dessert.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/coffee.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Food&Drink/beers.svg?v=0.3.7'
    ]
  },
  {
    name: '其他',
    list: [
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Others/social.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Others/clothes.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Others/yes.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Others/boom.svg?v=0.3.7',
      'https://zdxblog.cn/image/assets/snowbird/0.4.1/illustrations/Minimalist/Others/wow.svg?v=0.3.7'
    ]
  }
]
