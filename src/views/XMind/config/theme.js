export default [
  {
    name: 'Rainbow',
    key: 'rainbow',
    dataList: [
      {
        background: 'linear-gradient(205deg, rgb(249, 66, 58) 0%, rgb(249, 66, 58) 33%, rgb(243, 211, 33) 33%, rgb(243, 211, 33) 66%, rgb(72, 106, 255) 66%, rgb(72, 106, 255) 100%)'
      },
      {
        background: 'linear-gradient(205deg, rgb(250, 129, 85) 0%, rgb(250, 129, 85) 33%, rgb(183, 200, 43) 33%, rgb(183, 200, 43) 66%, rgb(117, 116, 188) 66%, rgb(117, 116, 188) 100%)'
      },
      {
        background: 'linear-gradient(205deg, rgb(231, 194, 192) 0%, rgb(231, 194, 192) 33%, rgb(216, 220, 175) 33%, rgb(216, 220, 175) 66%, rgb(176, 192, 208) 66%, rgb(176, 192, 208) 100%)'
      },
      {
        background: 'linear-gradient(205deg, rgb(211, 189, 108) 0%, rgb(211, 189, 108) 33%, rgb(118, 177, 138) 33%, rgb(118, 177, 138) 66%, rgb(81, 146, 164) 66%, rgb(81, 146, 164) 100%)'
      },
      {
        background: 'linear-gradient(205deg, rgb(242, 217, 110) 0%, rgb(242, 217, 110) 33%, rgb(250, 129, 85) 33%, rgb(250, 129, 85) 66%, rgb(197, 63, 77) 66%, rgb(197, 63, 77) 100%)'
      },
      {
        background: 'linear-gradient(205deg, rgb(11, 93, 153) 0%, rgb(11, 93, 153) 33%, rgb(123, 64, 131) 33%, rgb(123, 64, 131) 66%, rgb(195, 65, 80) 66%, rgb(195, 65, 80) 100%)'
      }
    ]
  },
  {
    name: 'Iris',
    key: 'iris',
    dataList: [
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(146, 87, 255)'
      },
      {
        background: 'rgb(157, 2, 234)'
      },
      {
        background: 'rgb(92, 20, 200)'
      },
      {
        background: 'rgb(46, 15, 107)'
      },
      {
        background: 'rgb(197, 174, 249)'
      }
    ]
  },
  {
    name: 'Energy',
    key: 'energy',
    dataList: [
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(242, 242, 242)'
      },
      {
        background: 'rgb(242, 40, 22)'
      },
      {
        background: 'rgb(242, 184, 7)'
      },
      {
        background: 'rgb(35, 62, 217)'
      },
      {
        background: 'rgb(13, 13, 13)'
      }
    ]
  },
  {
    name: 'Dancing',
    key: 'dancing',
    dataList: [
      {
        background: 'rgb(78, 96, 239)'
      },
      {
        background: 'rgb(235, 71, 88)'
      },
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(255, 248, 224)'
      },
      {
        background: 'rgb(170, 14, 29)'
      },
      {
        background: 'rgb(54, 48, 38)'
      }
    ]
  },
  {
    name: 'Code',
    key: 'code',
    dataList: [
      {
        background: 'rgb(255, 240, 184)'
      },
      {
        background: 'rgb(203, 255, 184)'
      },
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(219, 143, 255)'
      },
      {
        background: 'rgb(138, 190, 255)'
      },
      {
        background: 'rgb(44, 45, 48)'
      }
    ]
  },
  {
    name: 'Sakura',
    key: 'sakura',
    dataList: [
      {
        background: 'rgb(255, 227, 232)'
      },
      {
        background: 'rgb(255, 220, 200)'
      },
      {
        background: 'rgb(255, 180, 182)'
      },
      {
        background: 'rgb(255, 169, 198)'
      },
      {
        background: 'rgb(209, 195, 189)'
      },
      {
        background: 'rgb(193, 207, 222)'
      }
    ]
  },
  {
    name: 'Kimono',
    key: 'kimono',
    dataList: [
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(255, 171, 170)'
      },
      {
        background: 'rgb(255, 123, 49)'
      },
      {
        background: 'rgb(140, 181, 255)'
      },
      {
        background: 'rgb(74, 81, 217)'
      },
      {
        background: 'rgb(25, 25, 89)'
      }
    ]
  },
  {
    name: 'Innocence',
    key: 'innocence',
    dataList: [
      {
        background: 'rgb(253, 201, 209)'
      },
      {
        background: 'rgb(234, 97, 138)'
      },
      {
        background: 'rgb(164, 208, 249)'
      },
      {
        background: 'rgb(79, 115, 186)'
      },
      {
        background: 'rgb(253, 248, 231)'
      },
      {
        background: 'rgb(60, 66, 68)'
      }
    ]
  },
  {
    name: 'Mint',
    key: 'mint',
    dataList: [
      {
        background: 'rgb(255, 255, 255)'
      },
      {
        background: 'rgb(196, 255, 249)'
      },
      {
        background: 'rgb(156, 234, 239)'
      },
      {
        background: 'rgb(104, 216, 214)'
      },
      {
        background: 'rgb(6, 175, 169)'
      },
      {
        background: 'rgb(4, 101, 98)'
      }
    ]
  },
  {
    name: 'ClassicsPrimary',
    key: 'classicsPrimary',
    dataList: [
      {
        background: '#3949AB'
      },
      {
        background: '#E53935'
      },
      {
        background: '#C0CA33'
      },
      {
        background: '#00897B'
      },
      {
        background: '#1E88E5'
      },
      {
        background: '#8E24AA'
      }
    ]
  }
]
