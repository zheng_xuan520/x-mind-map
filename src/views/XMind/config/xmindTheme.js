export default {
  centralTopic: {
    id: '1a03c573-75aa-4cdc-bc11-f0c8a6da7ca3',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '30pt',
      'fo:font-weight': '500',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'center',
      'svg:fill': '#000229',
      'fill-pattern': 'solid',
      'line-width': '3pt',
      'line-color': '#000229',
      'line-pattern': 'solid',
      'border-line-color': 'inherited',
      'border-line-width': '0pt',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'line-class': 'org.xmind.branchConnection.curve',
      'arrow-end-class': 'org.xmind.arrowShape.none',
      'alignment-by-level': 'inherited'
    }
  },
  mainTopic: {
    id: '7abac503-0b52-41e0-ac73-7f4cfe6fb8f1',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '18pt',
      'fo:font-weight': '500',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'left',
      'svg:fill': 'inherited',
      'fill-pattern': 'solid',
      'line-width': '2pt',
      'alignment-by-level': 'inherited',
      'line-color': 'inherited',
      'line-pattern': 'inherited',
      'border-line-color': 'inherited',
      'border-line-width': '0pt',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'line-class': 'org.xmind.branchConnection.roundedElbow',
      'arrow-end-class': 'inherited'
    }
  },
  subTopic: {
    id: 'a497005b-3e11-4e63-9be1-046161e79d0b',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '14pt',
      'fo:font-weight': '400',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'left',
      'svg:fill': 'inherited',
      'fill-pattern': 'solid',
      'alignment-by-level': 'inherited',
      'line-width': '2pt',
      'line-color': 'inherited',
      'line-pattern': 'inherited',
      'border-line-color': 'inherited',
      'border-line-width': '0pt',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'line-class': 'org.xmind.branchConnection.roundedElbow',
      'arrow-end-class': 'inherited'
    }
  },
  calloutTopic: {
    id: 'b46ca9c2-f331-4167-be76-50d2244bfad1',
    properties: {
      'fo:font-family': 'NeverMind',
      'line-class': 'org.xmind.branchConnection.roundedElbow',
      'alignment-by-level': 'inherited',
      'fo:font-size': '14pt',
      'fo:font-weight': '400',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'left',
      'svg:fill': '#000229',
      'fill-pattern': 'solid',
      'line-width': 'inherited',
      'line-color': 'inherited',
      'line-pattern': 'inherited',
      'border-line-color': '#000229',
      'border-line-width': 'inherited',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'arrow-end-class': 'inherited'
    }
  },
  summaryTopic: {
    id: '33485c6a-7d05-4f77-a919-7dff6a211e47',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '14pt',
      'fo:font-weight': '400',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'left',
      'svg:fill': '#000229',
      'fill-pattern': 'none',
      'line-width': 'inherited',
      'line-color': 'inherited',
      'line-pattern': 'inherited',
      'border-line-color': '#000229',
      'border-line-width': 'inherited',
      'alignment-by-level': 'inherited',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'line-class': 'org.xmind.branchConnection.roundedElbow',
      'arrow-end-class': 'inherited'
    }
  },
  floatingTopic: {
    id: '8ac5fb02-5308-410a-bb34-99d755c58ad0',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '14pt',
      'fo:font-weight': '500',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'left',
      'alignment-by-level': 'inherited',
      'svg:fill': '#EEEBEE',
      'fill-pattern': 'solid',
      'line-width': '2pt',
      'line-color': 'inherited',
      'line-pattern': 'solid',
      'border-line-color': '#EEEBEE',
      'border-line-width': '0pt',
      'border-line-pattern': 'inherited',
      'shape-class': 'org.xmind.topicShape.roundedRect',
      'line-class': 'org.xmind.branchConnection.roundedElbow',
      'arrow-end-class': 'org.xmind.arrowShape.none'
    }
  },
  importantTopic: {
    id: '69c8063a-b524-4fdf-8d8c-32d46519fe9b',
    properties: {
      'fo:font-weight': 'bold',
      'border-line-width': '0',
      'svg:fill': '#460400',
      'fill-pattern': 'solid',
      'border-line-color': '#460400'
    }
  },
  minorTopic: {
    id: '15d45f75-3764-4836-b847-0ada2f7c8d8b',
    properties: {
      'fo:font-weight': 'bold',
      'border-line-width': '0',
      'svg:fill': '#703D00',
      'fill-pattern': 'solid',
      'border-line-color': '#703D00'
    }
  },
  expiredTopic: {
    id: 'a912cb47-15e9-4590-b610-495851dc02c9',
    properties: {
      'fo:text-decoration': 'line-through',
      'fill-pattern': 'none'
    }
  },
  boundary: {
    id: 'a5f27307-dfbe-4e69-ac2e-e917ca8f6ab1',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '14pt',
      'fo:font-weight': '400',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'center',
      'svg:fill': '#000229',
      'svg:opacity': '0.2',
      'fill-pattern': 'solid',
      'line-width': '2',
      'line-color': '#000229',
      'line-pattern': 'dash',
      'shape-class': 'org.xmind.boundaryShape.roundedRect'
    }
  },
  summary: {
    id: 'f7ca7644-8d43-4566-814c-943a1c938a48',
    properties: {
      'line-width': '2pt',
      'line-color': '#000229',
      'line-pattern': 'solid',
      'shape-class': 'org.xmind.summaryShape.round'
    }
  },
  relationship: {
    id: '3f839c09-598a-4656-a4b9-74af42a26948',
    properties: {
      'fo:font-family': 'NeverMind',
      'fo:font-size': '13pt',
      'fo:font-weight': '400',
      'fo:font-style': 'normal',
      'fo:color': 'inherited',
      'fo:text-transform': 'manual',
      'fo:text-decoration': 'none',
      'fo:text-align': 'center',
      'line-width': '2',
      'line-color': '#000229',
      'line-pattern': 'dash',
      'shape-class': 'org.xmind.relationshipShape.curved',
      'arrow-begin-class': 'org.xmind.arrowShape.none',
      'arrow-end-class': 'org.xmind.arrowShape.triangle'
    }
  },
  map: {
    id: '546499e2-c6b5-4890-b1f7-233c9216ffb8',
    properties: {
      'svg:fill': '#ffffff',
      'multi-line-colors': '#F9423A #F6A04D #F3D321 #00BC7B #486AFF #4D49BE',
      'color-list': '#000229 #1F2766 #52CC83 #4D86DB #99142F #245570',
      'line-tapered': 'none'
    }
  },
  skeletonThemeId: 'db4a5df4db39a8cd1310ea55ea',
  colorThemeId: 'Rainbow-#000229-MULTI_LINE_COLORS'
}
