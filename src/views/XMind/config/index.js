import { randomId } from '../utils'

export const FONTFAMILYOPTIONS = [
  {
    label: '微软雅黑',
    value: "微软雅黑, 'Microsoft YaHei'"
  },
  {
    label: '楷体',
    value: '楷体, 楷体_GB2312, SimKai, STKaiti'
  },
  {
    label: '黑体',
    value: '黑体'
  },
  {
    label: '隶书',
    value: '隶书, SimLi'
  },
  {
    label: 'Arial',
    value: 'arial, helvetica, sans-serif'
  },
  {
    label: 'arialBlack',
    value: "'arial black', 'avant garde'"
  },
  {
    label: 'Comic Sans Ms',
    value: "'comic sans ms'"
  },
  {
    label: 'Impact',
    value: 'impact, chicago'
  },
  {
    label: 'Times New Roman',
    value: "'times new roman'"
  },
  {
    label: 'Sans-Serif',
    value: 'sans-serif'
  },
  {
    label: 'consola',
    value: 'consola'
  },
  {
    label: 'lato',
    value: 'lato'
  },
  {
    label: 'nevermind',
    value: 'nevermind'
  },
  {
    label: 'nevermindhand',
    value: 'nevermindhand'
  },
  {
    label: 'nevermindhand alo',
    value: 'nevermindhand,allseto'
  },
  {
    label: 'audiowide',
    value: 'audiowide'
  },
  {
    label: 'Arvo',
    value: 'Arvo'
  }
]

export const FONTMAXWIDTHOPTIONS = new Array(10).fill(null).map((_o, index) => {
  return {
    label: `${(index + 3) * 100}px`,
    value: (index + 3) * 100
  }
})

export const FONTSIZEOPTIONS = (function () {
  const cache = []
  for (let i = 8; i <= 64; i++) {
    if (i % 2 === 0) {
      cache.push({
        label: i + 'px',
        value: i
      })
    }
  }
  return cache
})()

export const STROKESTYLEOPTIONS = [
  {
    label: '实线',
    value: 'solid'
  },
  {
    label: '虚线',
    value: 'dashed'
  }
]

export const STROKEWIDTHOPTIONS = [
  {
    label: '无边框',
    value: 0
  },
  {
    label: '极细',
    value: 1
  },
  {
    label: '细',
    value: 2
  },
  {
    label: '中等',
    value: 3
  },
  {
    label: '粗',
    value: 4
  },
  {
    label: '极粗',
    value: 5
  }
]

export const TEXTDECARATIONOPTIONS = [
  {
    label: '默认',
    value: 'none'
  },
  {
    label: '中划线',
    value: 'line-through'
  },
  {
    label: '下划线',
    value: 'underline'
  }
]

export const EDGELINESIZEOPTIONS = [
  {
    label: '普通',
    options: [
      {
        value: 1,
        label: '极细'
      },
      {
        value: 2,
        label: '细'
      },
      {
        value: 3,
        label: '中等'
      },
      {
        value: 5,
        label: '粗'
      },
      {
        value: 8,
        label: '极粗'
      }
    ]
  },
  {
    label: '线条渐细',
    options: [
      {
        value: 'a-5',
        label: '极细-线条渐细'
      },
      {
        value: 'a-10',
        label: '细-线条渐细'
      },
      {
        value: 'a-15',
        label: '中等-线条渐细'
      },
      {
        value: 'a-20',
        label: '粗-线条渐细'
      },
      {
        value: 'a-25',
        label: '极粗-线条渐细'
      }
    ]
  }
]

export const COULDRESETFILEDS = [
  'fontFamily',
  'fontSize',
  'fontWeight',
  'fontStyle',
  'textDecoration',
  'textColor',
  'strokeColor',
  'strokeStyle',
  'strokeWidth',
  'backgroundColor',
  'lineStyle',
  'lineWidth',
  'lineColor',
  'horizontalOutter',
  'textDirection'
]

export const debounceUpdateFileds = ['textColor', 'strokeColor', 'backgroundColor', 'lineColor']

export const TEMPLATE_JSON = {
  text: 'Central Topic',
  children: [
    {
      text: 'Main Topic',
      children: null,
      _id: 'cichw15132433792407696',
      expand: true,
      childCount: 0,
      isRoot: false,
      customStyle: {
        fontFamily: null
      }
    },
    {
      text: 'Main Topic',
      children: null,
      _id: 'mbqrk2547528258419518',
      expand: true,
      childCount: 0,
      isRoot: false,
      customStyle: {
        fontFamily: null
      }
    },
    {
      text: 'Main Topic',
      children: null,
      _id: 'nfrna6816880902735392',
      expand: true,
      childCount: 0,
      isRoot: false,
      customStyle: {
        fontFamily: null
      }
    },
    {
      text: 'Main Topic',
      children: null,
      _id: 'ylijy29107450186334205',
      expand: true,
      childCount: 0,
      isRoot: false,
      customStyle: {
        fontFamily: null
      }
    }
  ],
  _id: 'auorz500097373075721',
  expand: true,
  childCount: 0,
  isRoot: true,
  customStyle: null,
  imageInfo: null
}

export const initXmindMapJson = () => {
  return {
    name: '未命名',
    id: randomId(),
    rootTopic: JSON.parse(JSON.stringify(TEMPLATE_JSON)),
    skeleton: 'mindMap-defaultSkeleton',
    theme: 'rainbow-0',
    eventTransform: null,
    gloabConfig: null,
    createTime: new Date()
  }
}

export const SCALES = [
  20, 50, 80, 100, 120, 150, 200, 300, 400, 500
]
