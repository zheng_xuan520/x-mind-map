export default [
  {
    name: '关系符号/括号/根号',
    backgroundColor: '#F9423A',
    textColor: '#ffffff',
    list: [
      { latex: '\\pm' },
      { latex: '\\times' },
      { latex: '\\div' },
      { latex: '\\neq' },
      { latex: '\\approx' },
      { latex: '\\equiv' },
      { latex: '\\geq' },
      { latex: '\\leq' },
      { latex: '\\sim' },
      { latex: '\\perp' },
      { latex: '\\sqrt{3}' },
      { latex: '\\sqrt[n]{3}' },
      { latex: '\\left ( \\frac{a}{b} \\right )' },
      { latex: '\\left[ \\frac{a}{b} \\right]' },
      { latex: '\\left \\langle \\frac{a}{b} \\right \\rangle' },
      { latex: '\\left\\{ \\frac{a}{b} \\right\\}' },
      { latex: '\\overbrace{ 1+2+\\cdots+100 }' },
      { latex: '\\underbrace{ a+b+\\cdots+z }' }
    ]
  },
  {
    name: '上标/下标/积分',
    backgroundColor: '#ff9c3d',
    textColor: '#ffffff',
    list: [
      { latex: 'a^4' },
      { latex: 'a_5' },
      { latex: 'a^{2+4}' },
      { latex: 'x_2^5' },
      { latex: 'x’' },
      { latex: '\\overbrace{11+12+\\cdots+100}' },
      { latex: '\\underbrace{a+b+\\cdots+z}' },
      { latex: '\\overleftarrow{a b}' },
      { latex: '\\overrightarrow{c d}' },
      { latex: '\\widehat{e f g}' },
      { latex: '\\overline{h i j}' },
      { latex: '\\sum_{k=1}^N k^2' },
      { latex: '\\prod_{i=1}^N x_i' }
    ]
  },
  {
    name: '分数/矩阵/多行列式',
    backgroundColor: '#00BC7B',
    textColor: '#000000',
    list: [
      { latex: '\\frac{1}{10}=0.1' },
      { latex: '\\tfrac{1}{10} = 0.1' },
      { latex: '\\dfrac{k}{k+9} = 0.1' },
      { latex: '\\dfrac{ \\tfrac{1}{2}[1-(\\tfrac{1}{2})^n] }{ 1-\\tfrac{1}{2} } = s_n' },
      { latex: '\\binom{n}{k}' },
      { latex: '\\tbinom{n}{k}' },
      { latex: '\\dbinom{n}{k}' }
    ]
  },
  {
    name: '三角学公式',
    backgroundColor: '#F3D321',
    textColor: '#000000',
    list: [
      { latex: '\\sin^{-1}\\theta' },
      { latex: 'e^{i \\theta}' },
      { latex: '\\left(\\frac{\\pi}{2}-\\theta \\right )' },
      { latex: '\\text{sin}^{2}\\frac{\\alpha}{2}=\\frac{1- \\text{cos}\\alpha}{2}' },
      { latex: '\\text{cos}^{2}\\frac{\\alpha}{2}=\\frac{1+ \\text{cos}\\alpha}{2}' },
      { latex: '\\text{tan}\\frac{\\alpha}{2}=\\frac{\\text{sin}\\alpha}{1+ \\text{cos}\\alpha}' },
      { latex: '\\sin \\alpha + \\sin \\beta =2 \\sin \\frac{\\alpha + \\beta}{2}\\cos \\frac{\\alpha – \\beta}{2}' },
      { latex: 'a^{2}=b^{2}+c^{2}-2bc\\cos A' }
    ]
  }
]
