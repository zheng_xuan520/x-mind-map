export default [
  {
    key: 'default',
    name: '默认图形'
  },
  {
    key: 'circleRect',
    name: '圆角矩形'
  },
  {
    key: 'ellipse',
    name: '椭圆'
  },
  {
    key: 'ellipticRect',
    name: '椭圆矩形'
  },
  {
    key: 'rect',
    name: '矩形'
  },
  {
    key: 'diamond',
    name: '菱形'
  },
  {
    key: 'singleBreakangle',
    name: '断角矩形'
  },
  {
    key: 'parallelogram',
    name: '平行四边形'
  },
  {
    key: 'polygonSide6',
    name: '外六边形'
  },
  {
    key: 'doubleRect',
    name: '双矩形'
  },
  {
    key: 'underline',
    name: '下划线'
  },
  {
    key: 'doubleUnderline',
    name: '双下划线'
  },
  {
    key: 'circle',
    name: '圆形'
  },
  {
    key: 'flower',
    name: '花瓣'
  },
  {
    key: 'heart',
    name: '心形'
  },
  {
    key: 'boom',
    name: '爆炸效果'
  },
  {
    key: 'doubleQuotes',
    name: '双引号'
  },
  {
    key: 'squareQuotes',
    name: '直角引号'
  }
]
