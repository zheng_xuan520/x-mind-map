
export default [
  {
    typeName: '优先级',
    type: 'youxianji',
    icons: [
      { icon: 'icon-number1', name: 'Priority 1' },
      { icon: 'icon-number2', name: 'Priority 2' },
      { icon: 'icon-number3', name: 'Priority 3' },
      { icon: 'icon-number4', name: 'Priority 4' },
      { icon: 'icon-number5', name: 'Priority 5' },
      { icon: 'icon-number6', name: 'Priority 6' },
      { icon: 'icon-number7', name: 'Priority 7' },
      { icon: 'icon-number8', name: 'Priority 8' },
      { icon: 'icon-number9', name: 'Priority 9' },
      { icon: 'icon-number10', name: 'Priority 10' },
      { icon: 'icon-number11', name: 'Priority 11' },
      { icon: 'icon-number12', name: 'Priority 12' },
      { icon: 'icon-number13', name: 'Priority 13' },
      { icon: 'icon-number14', name: 'Priority 14' },
      { icon: 'icon-number15', name: 'Priority 15' },
      { icon: 'icon-number16', name: 'Priority 16' }
    ]
  },
  {
    typeName: '任务',
    type: 'renwu',
    icons: [
      { icon: 'icon-weikaishi1', name: 'Task Starting' },
      { icon: 'icon-circular-pie-1', name: '1/8 Done' },
      { icon: 'icon-circular-pie-6', name: '3/8 Done' },
      { icon: 'icon-circular-pie-3', name: '1/2 Done' },
      { icon: 'icon-circular-pie-4', name: '5/8 Done' },
      { icon: 'icon-circular-pie-5', name: '3/4 Done' },
      { icon: 'icon-circular-pie-7', name: '7/8 Done' },
      { icon: 'icon-wancheng', name: 'Task Done' }
    ]
  },
  {
    typeName: '旗帜',
    type: 'qizhi',
    icons: [
      { icon: 'icon-flag_circle_fill', name: 'Red Flag' },
      { icon: 'icon-a-flag_circle_fill1', name: 'Orange Flag' },
      { icon: 'icon-a-flag_circle_fill2', name: 'Yellow Flag' },
      { icon: 'icon-a-flag_circle_fill3', name: 'Green Flag' },
      { icon: 'icon-a-flag_circle_fill4', name: 'Blue Flag' },
      { icon: 'icon-a-flag_circle_fill5', name: 'Indigo Flag' },
      { icon: 'icon-a-flag_circle_fill6', name: 'Purple Flag' },
      { icon: 'icon-a-flag_circle_fill7', name: 'Grey Flag' }
    ]
  },
  {
    typeName: '星星',
    type: 'xingxing',
    icons: [
      { icon: 'icon-star-circle', name: 'Red Star' },
      { icon: 'icon-a-star-circle1', name: 'Orange Star' },
      { icon: 'icon-a-star-circle2', name: 'Yellow Star' },
      { icon: 'icon-a-star-circle3', name: 'Green Star' },
      { icon: 'icon-a-star-circle4', name: 'Blue Star' },
      { icon: 'icon-a-star-circle5', name: 'Indigo Star' },
      { icon: 'icon-a-star-circle6', name: 'Purple Star' },
      { icon: 'icon-a-star-circle7', name: 'Grey Star' }
    ]
  },
  {
    typeName: '人像',
    type: 'renxiang',
    icons: [
      { icon: 'icon-user-filling', name: 'Red Avatar' },
      { icon: 'icon-a-user-filling1', name: 'Orange Avatar' },
      { icon: 'icon-a-user-filling2', name: 'Yellow Avatar' },
      { icon: 'icon-a-user-filling3', name: 'Green Avatar' },
      { icon: 'icon-a-user-filling4', name: 'Blue Avatar' },
      { icon: 'icon-a-user-filling5', name: 'Indigo Avatar' },
      { icon: 'icon-a-user-filling6', name: 'Purple Avatar' },
      { icon: 'icon-a-user-filling7', name: 'Grey Avatar' }
    ]
  },
  {
    typeName: '符号',
    type: 'fuhao',
    icons: [
      { icon: 'icon-dianzan1-mianxing', name: 'Like' },
      { icon: 'icon-dianzan3-mianxing', name: 'DisLike' },
      { icon: 'icon-xing-circle', name: 'Ping' },
      { icon: 'icon-tag_circle_fill', name: 'Tag' },
      { icon: 'icon-twitter-circle-fill', name: 'Twitter' },
      { icon: 'icon-alert-circle', name: 'Alert' },
      { icon: 'icon-error', name: 'Error' },
      { icon: 'icon-music-circle', name: 'Music' },
      { icon: 'icon-pencil-circle', name: 'Pencil' },
      { icon: 'icon-cloud-circle', name: 'Cloud' },
      { icon: 'icon-heart-circle', name: 'Heart' },
      { icon: 'icon-tel-circle', name: 'Phone' },
      { icon: 'icon-circle-ing', name: 'More' },
      { icon: 'icon-circle_lightning_fill', name: 'Lighting' },
      { icon: 'icon-flight_circle', name: 'Flight' },
      { icon: 'icon-star-circle1', name: 'Star' },
      { icon: 'icon-wenhao', name: 'Question' }
    ]
  }
]
