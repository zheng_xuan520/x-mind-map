/**
 * 生成圆形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  return `M${x} ${y + height / 2} A ${width / 2} ${height / 2} 0 1 1 ${x} ${y + height / 2 + 1}z`
}

/**
 * 生成圆形内边距处理
 * @returns
 */
function shapeGetPadding (width, height, paddingTB = 15, paddingLR = 15) {
  const R = Math.pow(width * width + height * height, 0.5) / 2
  return {
    paddingT: (R - height / 2) + paddingTB,
    paddingR: (R - width / 2) + paddingLR,
    paddingB: (R - height / 2) + paddingTB,
    paddingL: (R - width / 2) + paddingLR
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
