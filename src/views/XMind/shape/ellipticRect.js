/**
 * 生成椭圆角矩形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const spacing = 10
  const radius = Math.min(5, width / 60)
  return `M${x} ${y + spacing + radius} A${radius} ${radius} 0 0 1 ${x + radius} ${y + spacing} Q${x + width / 2} ${y - spacing} ${x + width - radius} ${y + spacing} A${radius} ${radius} 0 0 1 ${x + width} ${y + spacing + radius} L${x + width} ${y + height - spacing - radius} A${radius} ${radius} 0 0 1 ${x + width - radius} ${y + height - spacing} Q${x + width / 2} ${y + height + spacing} ${x + radius} ${y + height - spacing} A${radius} ${radius} 0 0 1 ${x} ${y + height - spacing - radius}Z`
}

function shapeGetPadding () {
  return {
    paddingT: 22,
    paddingB: 22
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
