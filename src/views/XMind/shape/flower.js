/**
 *
 * @param {*} petals 花瓣数量
 * @param {*} radius 花朵半径
 * @param {*} centerX 花朵圆心x
 * @param {*} centerY 花朵圆心y
 * @param {*} angle 花朵起始角度
 * @returns
 */
function generateFlowerPath (petals, radius, centerX, centerY, angle) {
  const flowerR = Math.sin(Math.PI / (petals * 2)) * radius
  radius -= flowerR
  let path = ''
  for (let i = 0; i <= petals; i++) {
    const radians = angle + i * (2 * Math.PI / petals)
    const x = centerX + radius * Math.cos(radians)
    const y = centerY + radius * Math.sin(radians)
    if (i === 0) {
      path += `M ${x} ${y}`
    } else {
      path += ` A ${flowerR} ${flowerR} 0 0 1 ${x} ${y}`
    }
  }
  return path
}

/**
 * 生成花朵图形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  return generateFlowerPath(7, width / 2, x + width / 2, y + height / 2, 0)
}

/**
 * 生成花朵图形内边距处理
 * @returns
 */
function shapeGetPadding (width, height) {
  const padding = 12
  const R = Math.pow(width * width + height * height, 0.5) / 2
  return {
    paddingT: (R - height / 2) + padding,
    paddingR: (R - width / 2) + padding,
    paddingB: (R - height / 2) + padding,
    paddingL: (R - width / 2) + padding
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
