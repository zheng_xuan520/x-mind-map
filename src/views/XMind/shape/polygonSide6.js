/**
 * 外六边形路径生成
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const BR = 8
  return `M${x} ${y + height / 2} L${x + BR} ${y} ${x + width - BR} ${y} ${x + width} ${y + height / 2} ${x + width - BR} ${y + height} ${x + BR} ${y + height} Z`
}

function shapeGetPadding () {
  const [paddingT, paddingR, paddingB, paddingL] = [15, 26, 15, 26]
  return {
    paddingT: paddingT,
    paddingR: paddingR,
    paddingB: paddingB,
    paddingL: paddingL
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
