/**
 * 生成圆角矩形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const BR = Math.min(height / 2, width / 2) // 圆角大小为高度或者宽度的一半
  const [startX, startY] = [x, y + BR]
  const circlePath = `A${BR} ${BR} 0 0 1`
  return `M${startX} ${startY} ${circlePath} ${x + BR} ${y} L${x + width - BR} ${y} ${circlePath} ${x + width} ${y + BR} L${x + width} ${y + height - BR} ${circlePath} ${x + width - BR} ${y + height} L${x + BR} ${y + height} ${circlePath} ${x} ${y + height - BR}Z`
}

/**
 * 生成椭圆内边距处理
 * @returns
 */
function shapeGetPadding (width, height, paddingTB, paddingLR) {
  return {
    paddingT: paddingTB || (height > width ? width : 0),
    paddingR: paddingLR || (width > height ? height : 0),
    paddingB: paddingTB || (height > width ? width : 0),
    paddingL: paddingLR || (width > height ? height : 0)
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
