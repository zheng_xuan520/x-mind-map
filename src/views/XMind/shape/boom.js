/**
 * 生成爆炸效果路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const [originalWidth, originalHeight] = [150, 111]
  const originalPath = 'M 60.8 95 C 60.8 95 63.4 107.9 63.4 107.9 C 63.4 107.9 74.5 93 74.5 93 C 74.5 93 79.7 100.4 79.7 100.4 C 79.7 100.4 84 95 84 95 C 84 95 92.4 107.9 92.4 107.9 C 92.4 107.9 94.7 93 94.7 93 C 94.7 93 102.2 100.4 102.2 100.4 C 102.2 100.4 104.5 93 104.5 93 C 104.5 93 120.3 114.3 120.3 114.3 C 120.3 114.3 116.5 91.7 116.5 91.7 C 116.5 91.7 132.5 98.7 132.5 98.7 C 132.5 98.7 122.7 78.6 122.7 78.6 C 122.7 78.6 134.9 74 134.9 74 C 134.9 74 126.2 63.9 126.2 63.9 C 126.2 63.9 152.5 56.9 152.5 56.9 C 152.5 56.9 127.8 51.7 127.8 51.7 C 127.8 51.7 132.5 39.9 132.5 39.9 C 132.5 39.9 122.7 42.5 122.7 42.5 C 122.7 42.5 137.8 20.6 137.8 20.6 C 137.8 20.6 111.9 34.7 111.9 34.7 C 111.9 34.7 111.9 22 111.9 22 C 111.9 22 100.3 31.9 100.3 31.9 C 100.3 31.9 100.3 22 100.3 22 C 100.3 22 90.8 27.8 90.8 27.8 C 90.8 27.8 86.8 3.7 86.8 3.7 C 86.8 3.7 79.7 29.3 79.7 29.3 C 79.7 29.3 74.5 18.5 74.5 18.5 C 74.5 18.5 71.1 29.3 71.1 29.3 C 71.1 29.3 63.4 5.4 63.4 5.4 C 63.4 5.4 56.7 29.3 56.7 29.3 C 56.7 29.3 44.5 19.1 44.5 19.1 C 44.5 19.1 44.5 34.7 44.5 34.7 C 44.5 34.7 36 31.9 36 31.9 C 36 31.9 36 36.3 36 36.3 C 36 36.3 12.7 31.9 12.7 31.9 C 12.7 31.9 28.8 49.3 28.8 49.3 C 28.8 49.3 19.1 52.9 19.1 52.9 C 19.1 52.9 26 56.9 26 56.9 C 26 56.9 3.1 63.9 3.1 63.9 C 3.1 63.9 30.9 70.3 30.9 70.3 C 30.9 70.3 19.1 84.2 19.1 84.2 C 19.1 84.2 34.3 80.2 34.3 80.2 C 34.3 80.2 28.8 104.4 28.8 104.4 C 28.8 104.4 47.4 89.2 47.4 89.2 C 47.4 89.2 47.4 98.7 47.4 98.7 C 47.4 98.7 60.8 95 60.8 95'
  const [startX, startY] = [x + 0.4053 * width, y + 0.8558 * height]
  const scale = width > height ? width / originalWidth : height / originalHeight
  const points = originalPath.split(' ')
  let k = 'x'
  points.forEach((p, index) => {
    if (!isNaN(Number(p))) {
      if (k === 'x') {
        points[index] = (p - 60.8) * scale + startX
      } else if (k === 'y') {
        points[index] = (p - 95) * scale + startY
      }
      k = k === 'x' ? 'y' : 'x'
    }
  })
  return points.join(' ')
}

/**
 * 爆炸效果内边距处理
 * @returns
 */
function shapeGetPadding (width, height) {
  const maxWidth = width >= height
  const spacing = Math.max(Math.max(width, height) * 0.5, 20)
  return {
    paddingT: maxWidth ? ((width + spacing * 2) / 150 * 111 - height) / 2 : spacing,
    paddingR: maxWidth ? spacing : ((height + spacing * 2) / 111 * 150 - width) / 2,
    paddingB: maxWidth ? ((width + spacing * 2) / 150 * 111 - height) / 2 : spacing,
    paddingL: maxWidth ? spacing : ((height + spacing * 2) / 111 * 150 - width) / 2
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
