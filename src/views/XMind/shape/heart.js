/**
 * 生成心形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const [originalWidth, originalHeight] = [421, 311]
  const originalPath = 'M 122.2 27.5 C 58.2 27.5 6.3 78.7 6.3 141.8 C 6.3 256.2 143.3 360.1 217 384.3 C 290.7 360.1 427.7 256.2 427.7 141.8 C 427.7 78.7 375.8 27.5 311.8 27.5 C 272.6 27.5 238 46.7 217 76.1 C 196 46.7 161.4 27.5 122.2 27.5 C 122.2 27.5 122.2 27.5 122.2 27.5'
  const [startX, startY] = [x + 0.29 * width, y]
  const scale = width > height ? width / originalWidth : height / originalHeight
  const points = originalPath.split(' ')
  let k = 'x'
  points.forEach((p, index) => {
    if (!isNaN(Number(p))) {
      if (k === 'x') {
        points[index] = (p - 122.2) * scale + startX
      } else if (k === 'y') {
        points[index] = (p - 27.5) * scale + startY
      }
      k = k === 'x' ? 'y' : 'x'
    }
  })
  return points.join(' ')
}

/**
 * 生成心形内边距处理
 * @returns
 */
function shapeGetPadding (width, height) {
  const maxWidth = width >= height
  const spacing = maxWidth ? 15 : 0.2 * height + 10
  const [X, Y] = [421, 356.5]
  return {
    paddingT: maxWidth ? ((width + spacing * 2) / X * Y - height) / 2 : spacing,
    paddingR: maxWidth ? spacing : ((height + spacing * 2) / Y * X - width) / 2,
    paddingB: maxWidth ? ((width + spacing * 2) / X * Y - height) / 2 : spacing,
    paddingL: maxWidth ? spacing : ((height + spacing * 2) / Y * X - width) / 2
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
