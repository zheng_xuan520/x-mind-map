/**
 * 生成下划线形状路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  return [
    `M${x} ${y + height} L${x + width} ${y + height}`,
    `M${x} ${y} h${width} v${height} h${-width} v${-height}`
  ]
}

export default {
  generateShapePath
}
