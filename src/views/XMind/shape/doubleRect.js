/**
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height, strokeWidth = 0) {
  const spacing = 4 + strokeWidth
  return `M${x} ${y} h${width - spacing} v${height - spacing} h${-width + spacing} v${-height + spacing} M${x + spacing} ${y + height - spacing} v${spacing} h${width - spacing} v${-height + spacing} h${-spacing} v${height - 2 * spacing}`
}

function shapeGetPadding () {
  return {
    paddingT: 12,
    paddingR: 18,
    paddingB: 16,
    paddingL: 18
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
