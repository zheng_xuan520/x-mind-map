/**
 * 生成直角角矩形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const [startX, startY] = [x, y]
  return `M${startX} ${startY} L${x + width} ${y} ${x + width} ${y + height} ${x} ${y + height}Z`
}

export default {
  generateShapePath
}
