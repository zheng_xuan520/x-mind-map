/**
 * 生成下划线形状路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height, strokeWidth = 0) {
  const spaciing = 3 + strokeWidth
  return [
    `M${x} ${y + height} L${x + width} ${y + height} M${x} ${y + height - spaciing} h${width}`,
    `M${x} ${y} h${width} v${height} h${-width} v${-height}`
  ]
}

/**
 * @returns
 */
function shapeGetPadding () {
  const [paddingT, paddingR, paddingB, paddingL] = [12, 26, 16, 26]
  return {
    paddingT: paddingT,
    paddingR: paddingR,
    paddingB: paddingB,
    paddingL: paddingL
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
