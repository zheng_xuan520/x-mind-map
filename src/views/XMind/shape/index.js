import defaultShap from './default' // 默认图形
import circleRect from './circleRect' // 圆角矩形
import rect from './rect' // 矩形
import diamond from './diamond' // 菱形
import parallelogram from './parallelogram' // 平行四边形
import circle from './circle' // 圆形
import ellipse from './ellipse' // 椭圆形
import polygonSide6 from './polygonSide6' // 外六边形
import flower from './flower' // 花瓣
import doubleRect from './doubleRect' // 双矩形
import heart from './heart' // 心形
import boom from './boom'
import underline from './underline'
import singleBreakangle from './singleBreakangle'
import ellipticRect from './ellipticRect'
import doubleUnderline from './doubleUnderline'
import doubleQuotes from './doubleQuotes'
import squareQuotes from './squareQuotes'

export default {
  default: defaultShap,
  circleRect,
  rect,
  diamond,
  parallelogram,
  circle,
  ellipse,
  polygonSide6,
  flower,
  doubleRect,
  heart,
  boom,
  underline,
  singleBreakangle,
  ellipticRect,
  doubleUnderline,
  doubleQuotes,
  squareQuotes
}
