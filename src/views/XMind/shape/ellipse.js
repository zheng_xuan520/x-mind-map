/**
 * 生成椭圆路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  return `M${x} ${y + height / 2} A ${width / 2} ${height / 2} 0 1 1 ${x} ${y + height / 2 + 1}z`
}

/**
 * 生成椭圆内边距处理
 * @returns
 */
function shapeGetPadding (_width, height) {
  const horizontalSpacing = Math.min(height * 1.6, 36)
  return {
    paddingT: Math.max(height / 1.5, 36),
    paddingR: horizontalSpacing,
    paddingB: Math.max(height / 1.5, 36),
    paddingL: horizontalSpacing
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
