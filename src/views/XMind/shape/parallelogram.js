/**
 * 生成平行四边形图形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const BR = 16
  width -= BR
  const [startX, startY] = [x + BR, y]
  return `M${startX} ${startY} ${x + width + BR} ${y} ${x + width} ${y + height} ${x} ${y + height}Z`
}

/**
 * 生成平行四边形内边距设置
 * @returns
 */
function shapeGetPadding () {
  const [paddingT, paddingR, paddingB, paddingL] = [10, 26, 10, 26]
  return {
    paddingT: paddingT,
    paddingR: paddingR,
    paddingB: paddingB,
    paddingL: paddingL
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
