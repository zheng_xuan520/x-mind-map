/**
 * 生成菱形路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  return `M${x} ${y + height / 2} L${x + width / 2} ${y} ${x + width} ${y + height / 2} ${x + width / 2} ${y + height} Z`
}

/**
 * 生成菱形内边距处理
 * @returns
 */
function shapeGetPadding (width, height) {
  const horizontalSpacing = height * 1.6
  const vertical = height / 2 + 8
  const ratio = horizontalSpacing / vertical
  const halfHeight = (horizontalSpacing + width / 2) / ratio
  return {
    paddingT: halfHeight - height / 2,
    paddingR: horizontalSpacing,
    paddingB: halfHeight - height / 2,
    paddingL: horizontalSpacing
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
