/**
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const [h, v] = [Math.min(18, width / 2), Math.min(24, height / 2)]
  const path = `M${x} ${y + v} v${-v} h${h} M${x + width - h} ${y + height} h${h} v${-v}`
  return [
    path,
    `M${x} ${y} h${width} v${height} h${-width} v${-height}`
  ]
}

function shapeGetPadding () {
  const [paddingT, paddingR, paddingB, paddingL] = [12, 24, 12, 24]
  return {
    paddingT: paddingT,
    paddingR: paddingR,
    paddingB: paddingB,
    paddingL: paddingL
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
