/**
 * 生成双引号形状路径
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height, strokeWidth) {
  const spacing = 5 + strokeWidth * 2
  const s = strokeWidth / 2
  function getCirclePath (x, y, r) {
    return `M${x} ${y} A${r} ${r} 0 1 1 ${x} ${y - 0.01}z`
  }
  function getUpPath (x, y) {
    return `M${x} ${y} Q${x - 0.5 * s} ${y - 5 * s} ${x + 4 * s} ${y - 7 * s}`
  }
  function getDownPath (x, y) {
    return `M${x} ${y} Q${x + 0.5 * s} ${y + 5 * s} ${x - 4 * s} ${y + 7 * s}`
  }
  const k = 2
  const path1 = `${getCirclePath(x + spacing, y + height / 2 + k, strokeWidth)} ${getCirclePath(x + spacing - strokeWidth / 2, y + height / 2 + k, strokeWidth / 2)} ${getUpPath(x + spacing - strokeWidth * 2.05, y + height / 2 + k)}`
  const path2 = `${getCirclePath(x + spacing + strokeWidth * 2 + 3, y + height / 2 + k, strokeWidth)} ${getCirclePath(x + spacing + strokeWidth * 2 + 3 - strokeWidth / 2, y + height / 2 + k, strokeWidth / 2)} ${getUpPath(x + spacing + strokeWidth * 2.05 + 3 - strokeWidth * 2, y + height / 2 + k)}`
  const path3 = `${getCirclePath(x + width - spacing / 2, y + height / 2 - k * 2, strokeWidth)} ${getCirclePath(x + width - spacing / 2 - strokeWidth / 2, y + height / 2 - k * 2, strokeWidth / 2)} ${getDownPath(x + width - spacing / 2.05, y + height / 2 - k * 2)}`
  const path4 = `${getCirclePath(x + width - spacing / 2 - strokeWidth * 2 - 3, y + height / 2 - k * 2, strokeWidth)} ${getCirclePath(x + width - spacing / 2 - strokeWidth / 2 - strokeWidth * 2 - 3, y + height / 2 - k * 2, strokeWidth / 2)} ${getDownPath(x + width - spacing / 2.05 - strokeWidth * 2.05 - 3, y + height / 2 - k * 2)}`
  return [
    `${path1}${path2}${path3}${path4}`,
    `M${x} ${y} h${width} v${height} h${-width} v${-height}`
  ]
}

/**
 * @returns
 */
function shapeGetPadding () {
  const [paddingT, paddingR, paddingB, paddingL] = [16, 32, 16, 32]
  return {
    paddingT: paddingT,
    paddingR: paddingR,
    paddingB: paddingB,
    paddingL: paddingL
  }
}

export default {
  generateShapePath,
  shapeGetPadding
}
