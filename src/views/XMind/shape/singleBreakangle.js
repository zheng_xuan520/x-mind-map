/**
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const breakSpacing = Math.min(20, Math.min(width, height) / 4.5)
  return `M${x} ${y} L${x + width - breakSpacing} ${y} ${x + width} ${y + breakSpacing} ${x + width} ${y + height} ${x} ${y + height} ${x} ${y} M ${x + width - breakSpacing} ${y} ${x + width - breakSpacing} ${y + breakSpacing} ${x + width} ${y + breakSpacing}`
}

export default {
  generateShapePath
}
