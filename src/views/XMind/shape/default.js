/**
 * 生成默认图形路径，圆角半径为6的圆角矩形
 * @param {*} x
 * @param {*} y
 * @param {*} width
 * @param {*} height
 */
function generateShapePath (x, y, width, height) {
  const BR = 6
  const [startX, startY] = [x, y + BR]
  const circlePath = `A${BR} ${BR} 0 0 1`
  return `M${startX} ${startY} ${circlePath} ${x + BR} ${y} L${x + width - BR} ${y} ${circlePath} ${x + width} ${y + BR} L${x + width} ${y + height - BR} ${circlePath} ${x + width - BR} ${y + height} L${x + BR} ${y + height} ${circlePath} ${x} ${y + height - BR}Z`
}

export default {
  generateShapePath
}
