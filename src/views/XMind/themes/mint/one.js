export default {
  backgroundColor: '#ffffff',
  thumbColor: '#046562',
  relationStyle: {
    lineColor: '#046562',
    textColor: '#046562'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#046562',
  summaryTextColor: '#046562',
  outBorderColor: '#046562',
  'fill-fill-fill': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      textColors: ['#000000', '#000000', '#000000'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(235, 251, 252, 1)', 'rgba(225, 247, 247, 1)', 'rgba(205, 239, 238, 1)'],
      textColors: ['#0E5357', '#14514F', '#03625F'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      textColors: ['#000000', '#000000', '#000000'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0E5357', '#14514F', '#03625F'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000', '#000000', '#000000'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0E5357', '#14514F', '#03625F'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000', '#000000', '#000000'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0E5357', '#14514F', '#03625F'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000', '#000000', '#000000'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0E5357', '#14514F', '#03625F'],
      strokeColors: ['#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  }
}
