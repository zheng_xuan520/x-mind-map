export default {
  backgroundColor: '#ffffff',
  thumbColor: '#06AFA9',
  relationStyle: {
    lineColor: '#06AFA9',
    textColor: '#06AFA9'
  },
  select: {
    strokeWidth: 2,
    stroke: '#06AFA9'
  },
  summaryLineColor: '#06AFA9',
  summaryTextColor: '#06AFA9',
  outBorderColor: '#06AFA9',
  'fill-fill-fill': {
    0: {
      colors: ['#06AFA9'],
      textColors: ['#000000'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#06AFA9'],
      textColors: ['#000000'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#06AFA9'],
      textColors: ['#000000'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#06AFA9'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#06AFA9'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#06AFA9'],
      expandColor: '#ffffff'
    }
  }
}
