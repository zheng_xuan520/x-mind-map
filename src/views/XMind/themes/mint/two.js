export default {
  backgroundColor: '#046562',
  thumbColor: '#ffffff',
  relationStyle: {
    lineColor: '#ffffff',
    textColor: '#ffffff'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#ffffff',
  summaryTextColor: '#ffffff',
  outBorderColor: '#ffffff',
  outBorderTextColor: '#046562',
  'fill-fill-fill': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#046562'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      textColors: ['#000000', '#000000', '#000000', '#000000'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(42, 132, 128, 1)', 'rgba(34, 128, 126, 1)', 'rgba(24, 124, 121, 1)', 'rgba(4, 116, 112, 1)'],
      textColors: ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#046562'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      textColors: ['#000000', '#000000', '#000000', '#000000'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#046562'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'],
      strokeColors: ['#C4FFF9', '#9CEAEF', '#68D8D6', '#06AFA9'],
      expandColor: '#ffffff'
    }
  }
}
