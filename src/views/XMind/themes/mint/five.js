export default {
  backgroundColor: '#9CEAEF',
  thumbColor: '#046562',
  relationStyle: {
    lineColor: '#046562',
    textColor: '#046562'
  },
  select: {
    strokeWidth: 2,
    stroke: '#046562'
  },
  summaryLineColor: '#046562',
  summaryTextColor: '#046562',
  outBorderColor: '#046562',
  'fill-fill-fill': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(29, 126, 123, 1)'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#046562'],
      textColors: ['#ffffff'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#9CEAEF'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#046562'],
      strokeColors: ['#046562'],
      expandColor: '#ffffff'
    }
  }
}
