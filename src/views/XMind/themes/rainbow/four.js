export default {
  backgroundColor: '#fff',
  thumbColor: '#4D86DB',
  relationStyle: {
    lineColor: '#4D86DB',
    textColor: '#4D86DB'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#4D86DB',
  summaryTextColor: '#4D86DB',
  outBorderColor: '#4D86DB',
  'fill-fill-fill': {
    0: {
      colors: ['#4D86DB'],
      textColors: ['#ffffff'],
      strokeColors: ['#4D86DB'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      textColors: ['#000000', '#000000', '#000000', '#000000', '#ffffff', '#ffffff'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#f6f2e2', '#eff0e1', 'rgba(228, 239, 232, 1)', 'rgba(221, 237, 235, 1)', 'rgba(220, 233, 237, 1)', 'rgba(228, 228, 236, 1)'],
      textColors: ['#4E4217', '#404421', '#25402E', '#224340', '#213C44', '#2A2A3B'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#4D86DB'],
      textColors: ['#ffffff'],
      strokeColors: ['#4D86DB'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      textColors: ['#000000', '#000000', '#000000', '#000000', '#ffffff', '#ffffff'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#4E4217', '#404421', '#25402E', '#224340', '#213C44', '#2A2A3B'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#4D86DB'],
      textColors: ['#ffffff'],
      strokeColors: ['#4D86DB'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#4E4217', '#404421', '#25402E', '#224340', '#213C44', '#2A2A3B'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#4D86DB'],
      strokeColors: ['#4D86DB'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#4E4217', '#404421', '#25402E', '#224340', '#213C44', '#2A2A3B'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#4D86DB'],
      strokeColors: ['#4D86DB'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#4E4217', '#404421', '#25402E', '#224340', '#213C44', '#2A2A3B'],
      strokeColors: ['#D3BD6C', '#ADB66B', '#76B18A', '#54A39B', '#5192A4', '#7878A0'],
      expandColor: '#ffffff'
    }
  }
}
