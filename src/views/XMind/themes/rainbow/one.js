export default {
  backgroundColor: '#fff',
  thumbColor: '#FF1493',
  relationStyle: {
    lineColor: '#00CED1',
    textColor: '#00CED1'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#000000',
  summaryTextColor: '#000000',
  outBorderColor: '#333333',
  'fill-fill-fill': {
    0: {
      colors: ['#092933'],
      textColors: ['#ffffff'],
      strokeColors: ['#092933'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      textColors: ['#ffffff', '#ffffff', '#000000', '#000000', '#ffffff', '#ffffff'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#fed9d8', '#fdecdb', '#ccf2e5', '#fdf6d3', '#dae1ff', '#dbdbf2'],
      textColors: ['#620703', '#613204', '#006642', '#605205', '#001266', '#1C1A4B'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#092933'],
      textColors: ['#ffffff'],
      strokeColors: ['#092933'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      textColors: ['#ffffff', '#ffffff', '#000000', '#000000', '#ffffff', '#ffffff'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620703', '#613204', '#006642', '#605205', '#001266', '#1C1A4B'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#092933'],
      textColors: ['#ffffff'],
      strokeColors: ['#092933'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620703', '#613204', '#006642', '#605205', '#001266', '#1C1A4B'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#092933'],
      strokeColors: ['#092933'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620703', '#613204', '#006642', '#605205', '#001266', '#1C1A4B'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#092933'],
      strokeColors: ['#092933'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620703', '#613204', '#006642', '#605205', '#001266', '#1C1A4B'],
      strokeColors: ['#F9423A', '#ff9c3d', '#00BC7B', '#F3D321', '#486AFF', '#4D49BE'],
      expandColor: '#ffffff'
    }
  }
}
