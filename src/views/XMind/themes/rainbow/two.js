export default {
  backgroundColor: '#fff',
  thumbColor: '#1F2766',
  relationStyle: {
    lineColor: '#1F2766',
    textColor: '#1F2766'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#1F2766',
  summaryTextColor: '#1F2766',
  outBorderColor: '#1F2766',
  'fill-fill-fill': {
    0: {
      colors: ['#1F2766'],
      textColors: ['#ffffff'],
      strokeColors: ['#1F2766'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#fee6dd', '#ffefd7', '#f1f4d5', '#ece0ee', '#e3e3f2', '#cceaf1'],
      textColors: ['#631C02', '#663C00', '#4C5312', '#3E2441', '#212144', '#005366'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#1F2766'],
      textColors: ['#ffffff'],
      strokeColors: ['#1F2766'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#631C02', '#663C00', '#4C5312', '#3E2441', '#212144', '#005366'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#631C02'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#1F2766'],
      textColors: ['#ffffff'],
      strokeColors: ['#1F2766'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#010e17', '#1f2b33', '#000000'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#631C02', '#663C00', '#4C5312', '#3E2441', '#212144', '#005366'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#631C02'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#1F2766'],
      strokeColors: ['#1F2766'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#010e17', '#1f2b33', '#000000'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#631C02', '#663C00', '#4C5312', '#3E2441', '#212144', '#005366'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#631C02'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#1F2766'],
      strokeColors: ['#1F2766'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#010e17', '#1f2b33', '#000000'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#631C02', '#663C00', '#4C5312', '#3E2441', '#212144', '#005366'],
      strokeColors: ['#FA8155', '#FFAD36', '#B7C82B', '#A165A8', '#7574BC', '#0098B9'],
      expandColor: '#631C02'
    }
  }
}
