export default {
  backgroundColor: '#fff',
  thumbColor: '#99142F',
  relationStyle: {
    lineColor: '#99142F',
    textColor: '#99142F'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#99142F',
  summaryTextColor: '#99142F',
  outBorderColor: '#99142F',
  'fill-fill-fill': {
    0: {
      colors: ['#99142F'],
      textColors: ['#ffffff'],
      strokeColors: ['#99142F'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff', '#ffffff', '#000000'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(252, 247, 226, 1)', 'rgba(255, 242, 227, 1)', 'rgba(254, 230, 221, 1)', 'rgba(237, 210, 215, 1)', 'rgba(243, 217, 219, 1)', 'rgba(255, 237, 231, 1)'],
      textColors: ['#5D4D08'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#99142F'],
      textColors: ['#ffffff'],
      strokeColors: ['#99142F'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff', '#ffffff', '#000000'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#5D4D08'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#99142F'],
      textColors: ['#ffffff'],
      strokeColors: ['#99142F'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#5D4D08'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#99142F'],
      strokeColors: ['#99142F'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#5D4D08'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#99142F'],
      strokeColors: ['#99142F'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#5D4D08'],
      strokeColors: ['#F2D96E', '#FFBE71', '#FA8155', '#A61D39', '#C53F4D', '#FFA787'],
      expandColor: '#ffffff'
    }
  }
}
