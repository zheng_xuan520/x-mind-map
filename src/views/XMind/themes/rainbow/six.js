export default {
  backgroundColor: '#fff',
  thumbColor: '#245570',
  relationStyle: {
    lineColor: '#245570',
    textColor: '#245570'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#245570',
  summaryTextColor: '#245570',
  outBorderColor: '#245570',
  'fill-fill-fill': {
    0: {
      colors: ['#245570'],
      textColors: ['#ffffff'],
      strokeColors: ['#245570'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      textColors: ['#ffffff'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(206, 223, 235, 1)', 'rgba(221, 220, 234, 1)', 'rgba(229, 217, 230, 1)', 'rgba(236, 216, 225, 1)', 'rgba(243, 217, 220, 1)', 'rgba(244, 222, 215, 1)'],
      textColors: ['#06395F', '#252243', '#402144', '#491C30', '#4D181E', '#502315'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#245570'],
      textColors: ['#ffffff'],
      strokeColors: ['#245570'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      textColors: ['#ffffff'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#06395F', '#252243', '#402144', '#491C30', '#4D181E', '#502315'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#245570'],
      textColors: ['#ffffff'],
      strokeColors: ['#245570'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#06395F', '#252243', '#402144', '#491C30', '#4D181E', '#502315'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#245570'],
      strokeColors: ['#245570'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#06395F', '#252243', '#402144', '#491C30', '#4D181E', '#502315'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#245570'],
      strokeColors: ['#245570'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#06395F', '#252243', '#402144', '#491C30', '#4D181E', '#502315'],
      strokeColors: ['#0B5D99', '#534E96', '#7B4083', '#A23E6A', '#C34150', '#CA5835'],
      expandColor: '#ffffff'
    }
  }
}
