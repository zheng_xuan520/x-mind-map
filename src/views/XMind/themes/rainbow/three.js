export default {
  backgroundColor: '#fff',
  thumbColor: '#52CC83',
  relationStyle: {
    lineColor: '#52CC83',
    textColor: '#52CC83'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#52CC83',
  summaryTextColor: '#52CC83',
  outBorderColor: '#52CC83',
  'fill-fill-fill': {
    0: {
      colors: ['#52CC83'],
      textColors: ['#101010'],
      strokeColors: ['#52CC83'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      textColors: ['#000000'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(250, 243, 242, 1)', 'rgba(253, 246, 240, 1)', 'rgba(247, 248, 239, 1)', 'rgba(238, 246, 245, 1)', 'rgba(239, 242, 246, 1)', 'rgba(244, 241, 244, 1)'],
      textColors: ['#491E1C', '#58340D', '#43461F', '#21443D', '#26323F', '#382C39'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#52CC83'],
      textColors: ['#101010'],
      strokeColors: ['#52CC83'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      textColors: ['#000000'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#491E1C', '#58340D', '#43461F', '#21443D', '#26323F', '#382C39'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#52CC83'],
      textColors: ['#101010'],
      strokeColors: ['#52CC83'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#491E1C', '#58340D', '#43461F', '#21443D', '#26323F', '#382C39'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#52CC83'],
      strokeColors: ['#52CC83'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#491E1C', '#58340D', '#43461F', '#21443D', '#26323F', '#382C39'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#52CC83'],
      strokeColors: ['#52CC83'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#491E1C', '#58340D', '#43461F', '#21443D', '#26323F', '#382C39'],
      strokeColors: ['#E7C2C0', '#F3D4B2', '#D8DCAF', '#A8D4CC', '#B0C0D0', '#C8BAC9'],
      expandColor: '#ffffff'
    }
  }
}
