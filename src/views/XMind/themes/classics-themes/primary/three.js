export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#D81B60',
    textColor: '#141414'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#D81B60',
  summaryTextColor: '#141414',
  outBorderColor: '#D81B60',
  outBorderTextColor: '#FFFFFF',
  'fill-fill-fill': {
    0: {
      colors: ['#C0CA33'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#C0CA33'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#C0CA33'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#C0CA33'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#C0CA33'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#C0CA33'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#C0CA33'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#C0CA33'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  }
}
