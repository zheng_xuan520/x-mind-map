export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FB8C00',
    textColor: '#141414'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FB8C00',
  summaryTextColor: '#141414',
  outBorderColor: '#FB8C00',
  outBorderTextColor: '#FFFFFF',
  'fill-fill-fill': {
    0: {
      colors: ['#00897B'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#00897B'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#00897B'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#00897B'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#00897B'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#00897B'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#00897B'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#00897B'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  }
}
