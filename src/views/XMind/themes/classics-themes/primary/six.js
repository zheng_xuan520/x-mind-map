export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#00ACC1',
    textColor: '#141414'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#00ACC1',
  summaryTextColor: '#141414',
  outBorderColor: '#00ACC1',
  outBorderTextColor: '#FFFFFF',
  'fill-fill-fill': {
    0: {
      colors: ['#8E24AA'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#8E24AA'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#8E24AA'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#8E24AA'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#8E24AA'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#8E24AA'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#8E24AA'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#8E24AA'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  }
}
