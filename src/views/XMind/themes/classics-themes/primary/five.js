export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#43A047',
    textColor: '#141414'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#43A047',
  summaryTextColor: '#141414',
  outBorderColor: '#43A047',
  outBorderTextColor: '#FFFFFF',
  'fill-fill-fill': {
    0: {
      colors: ['#1E88E5'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#1E88E5'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#1E88E5'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#1E88E5'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#1E88E5'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#1E88E5'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#1E88E5'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#1E88E5'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  }
}
