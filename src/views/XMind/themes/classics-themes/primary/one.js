export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#00897B',
    textColor: '#141414'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#00897B',
  summaryTextColor: '#141414',
  outBorderColor: '#00897B',
  outBorderTextColor: '#FFFFFF',
  'fill-fill-fill': {
    0: {
      colors: ['#3949AB'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#3949AB'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#3949AB'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#3949AB'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#EEEEEE'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#3949AB'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#3949AB'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#3949AB'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#3949AB'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#141414'],
      strokeColors: ['#141414'],
      expandColor: '#FFFFFF',
      collapseColor: '#141414'
    }
  }
}
