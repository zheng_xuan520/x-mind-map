export default {
  backgroundColor: '#FFABAA',
  thumbColor: '#191959',
  relationStyle: {
    lineColor: '#191959',
    textColor: '#191959'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#191959',
  summaryTextColor: '#191959',
  outBorderColor: '#191959',
  'fill-fill-fill': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 129, 127, 1)'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFABAA'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    }
  }
}
