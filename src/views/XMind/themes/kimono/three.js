export default {
  backgroundColor: '#ffffff',
  thumbColor: '#4A51D9',
  relationStyle: {
    lineColor: '#4A51D9',
    textColor: '#4A51D9'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#4A51D9',
  summaryTextColor: '#4A51D9',
  outBorderColor: '#4A51D9',
  'fill-fill-fill': {
    0: {
      colors: ['#4A51D9'],
      textColors: ['#ffffff'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#8CB5FF'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#4A51D9'],
      textColors: ['#ffffff'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#8CB5FF'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#4A51D9'],
      textColors: ['#ffffff'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#4A51D9'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#4A51D9'],
      expandColor: '#ffffff'
    }
  }
}
