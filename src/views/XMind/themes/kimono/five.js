export default {
  backgroundColor: '#8CB5FF',
  thumbColor: '#191959',
  relationStyle: {
    lineColor: '#191959',
    textColor: '#191959'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#191959',
  summaryTextColor: '#191959',
  outBorderColor: '#191959',
  'fill-fill-fill': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    1: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    normal: {
      colors: ['rgba(50, 50, 114, 1)'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    1: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#8CB5FF'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#191959'],
      strokeColors: ['#191959'],
      expandColor: '#191959',
      collapseColor: '#191959'
    }
  }
}
