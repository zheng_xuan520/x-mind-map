export default {
  backgroundColor: '#191959',
  thumbColor: '#FF7B31',
  relationStyle: {
    lineColor: '#FF7B31',
    textColor: '#FF7B31'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FF7B31',
  summaryTextColor: '#FF7B31',
  outBorderColor: '#FF7B31',
  'fill-fill-fill': {
    0: {
      colors: ['#FF7B31'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FF7B31'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FF7B31'],
      textColors: ['#191959'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#191959'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FF7B31'],
      expandColor: '#FF7B31',
      collapseColor: '#FF7B31'
    }
  }
}
