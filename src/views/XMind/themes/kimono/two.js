export default {
  backgroundColor: '#191959',
  thumbColor: '#ffffff',
  relationStyle: {
    lineColor: '#ffffff',
    textColor: '#ffffff'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#ffffff',
  summaryTextColor: '#ffffff',
  outBorderColor: '#ffffff',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#191959'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 238, 238, 1)', 'rgba(255, 229, 214, 1)', 'rgba(232, 240, 255, 1)', 'rgba(219, 220, 247, 1)'],
      textColors: ['#660100', '#662400', '#002466', '#111454'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#191959'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      textColors: ['#000000', '#000000', '#000000', '#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#191959'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#191959'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#191959'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFABAA', '#FF7B31', '#8CB5FF', '#4A51D9'],
      expandColor: '#ffffff'
    }
  }
}
