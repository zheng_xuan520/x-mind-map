export default {
  backgroundColor: '#fff',
  thumbColor: '#9D02EA',
  relationStyle: {
    lineColor: '#9D02EA',
    textColor: '#9D02EA'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#9D02EA',
  summaryTextColor: '#9D02EA',
  outBorderColor: '#9D02EA',
  'fill-fill-fill': {
    0: {
      colors: ['#9D02EA'],
      textColors: ['#ffffff'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#5C14C8'],
      textColors: ['#ffffff'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(167, 133, 246, 1)'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#9D02EA'],
      textColors: ['#ffffff'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#5C14C8'],
      textColors: ['#ffffff'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#9D02EA'],
      textColors: ['#ffffff'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#5C14C8'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#9D02EA'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#5C14C8'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#9D02EA'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#9D02EA'],
      expandColor: '#ffffff'
    }
  }
}
