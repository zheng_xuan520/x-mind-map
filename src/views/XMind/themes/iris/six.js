export default {
  backgroundColor: '#5c14c8',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FFFFFF',
    textColor: '#FFFFFF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFFFFF',
  summaryTextColor: '#FFFFFF',
  outBorderColor: '#FFFFFF',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#5c14c8'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  }
}
