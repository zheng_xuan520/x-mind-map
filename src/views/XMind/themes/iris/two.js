export default {
  backgroundColor: '#2e0f6b',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FFFFFF',
    textColor: '#FFFFFF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFFFFF',
  summaryTextColor: '#FFFFFF',
  outBorderColor: '#FFFFFF',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      textColors: ['#ffffff', '#ffffff', '#ffffff', '#000000'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(233, 221, 255, 1)', 'rgba(235, 204, 251, 1)', 'rgba(222, 208, 244, 1)', 'rgba(243, 239, 254, 1)'],
      textColors: ['#230066', '#430065', '#2A095C', '#21075E'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      textColors: ['#ffffff', '#ffffff', '#ffffff', '#000000'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#2e0f6b'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#9257FF', '#9D02EA', '#5C14C8', '#C5AEF9'],
      expandColor: '#ffffff'
    }
  }
}
