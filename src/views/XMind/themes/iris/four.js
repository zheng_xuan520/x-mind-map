export default {
  backgroundColor: '#c5aef9',
  thumbColor: '#2E0F6B',
  relationStyle: {
    lineColor: '#2E0F6B',
    textColor: '#2E0F6B'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#2E0F6B',
  summaryTextColor: '#2E0F6B',
  outBorderColor: '#2E0F6B',
  'fill-fill-fill': {
    0: {
      colors: ['#2E0F6B'],
      textColors: ['#ffffff'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#5C14C8'],
      textColors: ['#ffffff'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(167, 133, 246, 1)'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#2E0F6B'],
      textColors: ['#ffffff'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#5C14C8'],
      textColors: ['#ffffff'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#2E0F6B'],
      textColors: ['#ffffff'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#5C14C8'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#5C14C8'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#5C14C8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#c5aef9'],
      textColors: ['#2E0F6B'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#2E0F6B'],
      expandColor: '#ffffff'
    }
  }
}
