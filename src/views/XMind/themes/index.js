import rainbow from './rainbow'
import iris from './iris'
import energy from './energy'
import dancing from './dancing'
import code from './code'
import kimono from './kimono'
import innocence from './innocence'
import sakura from './sakura'
import mint from './mint'
import classicsPrimary from './classics-themes/primary'

// 关系连线样式二次更新
[rainbow,
  iris,
  energy,
  dancing,
  code,
  kimono,
  innocence,
  sakura,
  mint,
  classicsPrimary
].forEach(themes => {
  themes.forEach(theme => {
    theme.relationStyle = {
      lineShape: 'curved',
      startPointShape: 'arrow-end-dot',
      endPointShape: 'arrow-end-triangle',
      lineStyle: 'dashed',
      lineWidth: 2,
      fontFamily: 'nevermind',
      fontSize: 12,
      ...theme.relationStyle
    }
    theme.summaryStyle = {
      lineColor: theme.summaryLineColor,
      textColor: theme.summaryTextColor,
      lineShape: 'straight',
      topicShape: 'default',
      topicFill: '#00000000',
      topicStrokeColor: theme.summaryLineColor,
      fontSize: 12,
      fontFamily: 'nevermind'
    }
  })
})

export default {
  rainbow,
  iris,
  energy,
  dancing,
  code,
  kimono,
  innocence,
  sakura,
  mint,
  classicsPrimary
}
