export default {
  backgroundColor: '#0D0D0D',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FFFFFF',
    textColor: '#FFFFFF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFFFFF',
  summaryTextColor: '#FFFFFF',
  outBorderColor: '#FFFFFF',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      textColors: ['#000000', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(59, 59, 59, 1)', 'rgba(252, 212, 208, 1)', 'rgba(252, 241, 205, 1)', 'rgba(211, 216, 247, 1)'],
      textColors: ['#ffffff', '#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      textColors: ['#000000', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2F2F2', '#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  }
}
