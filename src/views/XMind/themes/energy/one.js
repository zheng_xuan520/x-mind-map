export default {
  backgroundColor: '#fff',
  thumbColor: '#0D0D0D',
  relationStyle: {
    lineColor: '#0D0D0D',
    textColor: '#0D0D0D'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#0D0D0D',
  summaryTextColor: '#0D0D0D',
  outBorderColor: '#0D0D0D',
  'fill-fill-fill': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F22816', '#F2B807', '#233ED9'],
      textColors: ['#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(252, 212, 208, 1)', 'rgba(252, 241, 205, 1)', 'rgba(211, 216, 247, 1)'],
      textColors: ['#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#F22816', '#F2B807', '#233ED9'],
      textColors: ['#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#600C05', '#634B02', '#0E1957'],
      strokeColors: ['#F22816', '#F2B807', '#233ED9'],
      expandColor: '#ffffff'
    }
  }
}
