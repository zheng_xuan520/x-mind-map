export default {
  backgroundColor: '#0D0D0D',
  thumbColor: '#F2B807',
  relationStyle: {
    lineColor: '#F2B807',
    textColor: '#F2B807'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#F2B807',
  summaryTextColor: '#F2B807',
  outBorderColor: '#F2B807',
  'fill-fill-fill': {
    0: {
      colors: ['#F2B807'],
      textColors: ['#000000'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['rgba(38, 38, 38, 1)'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#F2B807'],
      textColors: ['#000000'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#F2B807'],
      textColors: ['#000000'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#FFFFFF00'],
      textColors: ['#F2B807'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F2B807'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  }
}
