export default {
  backgroundColor: '#fff',
  thumbColor: '#233ED9',
  relationStyle: {
    lineColor: '#233ED9',
    textColor: '#233ED9'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#233ED9',
  summaryTextColor: '#233ED9',
  outBorderColor: '#233ED9',
  'fill-fill-fill': {
    0: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(214, 216, 217, 1)'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  }
}
