export default {
  backgroundColor: '#0D0D0D',
  thumbColor: '#F22816',
  relationStyle: {
    lineColor: '#F22816',
    textColor: '#F22816'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#F22816',
  summaryTextColor: '#F22816',
  outBorderColor: '#F22816',
  'fill-fill-fill': {
    0: {
      colors: ['#F22816'],
      textColors: ['#ffffff'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['rgba(38, 38, 38, 1)'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#F22816'],
      textColors: ['#ffffff'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#F22816'],
      textColors: ['#ffffff'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#FFFFFF00'],
      textColors: ['#F22816'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    1: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    },
    normal: {
      colors: ['FFFFFF00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#F22816'],
      expandColor: '#9257ff',
      collapseColor: '#9257ff'
    }
  }
}
