export default {
  backgroundColor: '#F2F2F2',
  thumbColor: '#0D0D0D',
  relationStyle: {
    lineColor: '#0D0D0D',
    textColor: '#0D0D0D'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#0D0D0D',
  summaryTextColor: '#0D0D0D',
  outBorderColor: '#0D0D0D',
  'fill-fill-fill': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(235, 235, 235, 1)'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#233ED9'],
      textColors: ['#ffffff'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#233ED9'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#233ED9'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#f2f2f2'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#0D0D0D'],
      expandColor: '#ffffff'
    }
  }
}
