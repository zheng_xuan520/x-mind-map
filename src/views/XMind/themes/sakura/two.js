export default {
  backgroundColor: '#FFDCC8',
  thumbColor: '#FFA9C6',
  relationStyle: {
    lineColor: '#FFA9C6',
    textColor: '#FFA9C6'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFA9C6',
  summaryTextColor: '#FFA9C6',
  outBorderColor: '#FFA9C6',
  'fill-fill-fill': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#D1C3BD'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 202, 172, 1)'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#D1C3BD'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFDCC8'],
      textColors: ['#FFA9C6'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    }
  }
}
