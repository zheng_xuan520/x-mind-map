export default {
  backgroundColor: '#FFE3E8',
  thumbColor: '#FFA9C6',
  relationStyle: {
    lineColor: '#FFA9C6',
    textColor: '#FFA9C6'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFA9C6',
  summaryTextColor: '#FFA9C6',
  outBorderColor: '#FFA9C6',
  'fill-fill-fill': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      textColors: ['#101010'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 218, 222, 1)', 'rgba(246, 221, 223, 1)', 'rgba(243, 223, 230, 1)'],
      textColors: ['#660002', '#3C2F29', '#233242'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      textColors: ['#101010'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#660002', '#3C2F29', '#233242'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFA9C6'],
      textColors: ['#101010'],
      strokeColors: ['#FFA9C6'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#660002', '#3C2F29', '#233242'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#101010'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#660002', '#3C2F29', '#233242'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFE3E8'],
      textColors: ['#101010'],
      strokeColors: ['#101010'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#660002', '#3C2F29', '#233242'],
      strokeColors: ['#FFB4B6', '#D1C3BD', '#C1CFDE'],
      expandColor: '#ffffff'
    }
  }
}
