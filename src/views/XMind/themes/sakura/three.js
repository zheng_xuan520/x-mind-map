export default {
  backgroundColor: '#FFB4B6',
  thumbColor: '#FFE3E8',
  relationStyle: {
    lineColor: '#FFE3E8',
    textColor: '#FFE3E8'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFE3E8',
  summaryTextColor: '#FFE3E8',
  outBorderColor: '#FFE3E8',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFE3E8'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFDCC8'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#FE8E91'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFE3E8'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FFDCC8'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFE3E8'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFB4B6'],
      textColors: ['#FFE3E8'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#FFFFFF00'],
      textColors: ['#101010'],
      strokeColors: ['#FFE3E8'],
      expandColor: '#ffffff'
    }
  }
}
