export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#EB4758',
  relationStyle: {
    lineColor: '#EB4758',
    textColor: '#EB4758'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#EB4758',
  summaryTextColor: '#EB4758',
  outBorderColor: '#EB4758',
  'fill-fill-fill': {
    0: {
      colors: ['#EB4758'],
      textColors: ['#ffffff'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#EB4758'],
      textColors: ['#ffffff'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#EB4758'],
      textColors: ['#ffffff'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#EB4758'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#EB4758'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#EB4758'],
      expandColor: '#ffffff'
    }
  }
}
