export default {
  backgroundColor: '#fff',
  thumbColor: '#AA0E1D',
  relationStyle: {
    lineColor: '#AA0E1D',
    textColor: '#AA0E1D'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#AA0E1D',
  summaryTextColor: '#AA0E1D',
  outBorderColor: '#AA0E1D',
  'fill-fill-fill': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#0D0D0D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#0D0D0D'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    }
  }
}
