export default {
  backgroundColor: '#363026',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FFFFFF',
    textColor: '#FFFFFF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFFFFF',
  summaryTextColor: '#FFFFFF',
  outBorderColor: '#FFFFFF',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#363026'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      textColors: ['#ffffff', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(59, 58, 78, 1)', 'rgba(90, 53, 48, 1)', 'rgba(94, 88, 75, 1)', 'rgba(77, 41, 36, 1)'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#363026'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      textColors: ['#ffffff', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#363026'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FFFFFF'],
      strokeColors: ['#4E60EF', '#EB4758', '#FFF8E0', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  }
}
