export default {
  backgroundColor: '#FFF8E0',
  thumbColor: '#363026',
  relationStyle: {
    lineColor: '#363026',
    textColor: '#363026'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#363026',
  summaryTextColor: '#363026',
  outBorderColor: '#363026',
  'fill-fill-fill': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 244, 208, 1)'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#AA0E1D'],
      textColors: ['#ffffff'],
      strokeColors: ['#AA0E1D'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FFF8E0'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    }
  }
}
