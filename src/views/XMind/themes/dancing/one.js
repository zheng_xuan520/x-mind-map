export default {
  backgroundColor: '#fff',
  thumbColor: '#363026',
  relationStyle: {
    lineColor: '#363026',
    textColor: '#363026'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#363026',
  summaryTextColor: '#363026',
  outBorderColor: '#363026',
  'fill-fill-fill': {
    0: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      textColors: ['#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(220, 223, 252, 1)', 'rgba(251, 218, 222, 1)', 'rgba(238, 207, 210, 1)'],
      textColors: ['#08115D', '#5C0A12', '#5E0710'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      textColors: ['#ffffff', '#ffffff', '#ffffff'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#08115D', '#5C0A12', '#5E0710'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['ffffff00'],
      textColors: ['#08115D', '#5C0A12', '#5E0710'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#08115D', '#5C0A12', '#5E0710'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#363026'],
      strokeColors: ['#363026'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#08115D', '#5C0A12', '#5E0710'],
      strokeColors: ['#4E60EF', '#EB4758', '#AA0E1D'],
      expandColor: '#ffffff'
    }
  }
}
