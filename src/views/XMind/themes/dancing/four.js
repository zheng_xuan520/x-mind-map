export default {
  backgroundColor: '#FFFFFF',
  thumbColor: '#4E60EF',
  relationStyle: {
    lineColor: '#4E60EF',
    textColor: '#4E60EF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#4E60EF',
  summaryTextColor: '#4E60EF',
  outBorderColor: '#4E60EF',
  'fill-fill-fill': {
    0: {
      colors: ['#4E60EF'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff'],
      textColors: ['#363026'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#4E60EF'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#363026'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#4E60EF'],
      textColors: ['#ffffff'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#4E60EF'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#ffffff'],
      textColors: ['#4E60EF'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#4E60EF'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#363026'],
      strokeColors: ['#233ED9'],
      expandColor: '#ffffff'
    }
  }
}
