import one from './one'
import two from './two'
import three from './three'
import four from './four'
import five from './five'
import six from './six'

export default [
  one,
  two,
  three,
  four,
  five,
  six
].map(o => {
  return {
    ...o,
    'plain-fill-plain': {
      0: o['fill-plain-plain-as-bgc']['0'],
      1: o['fill-fill-fill']['1'],
      normal: o['fill-plain-plain-as-bgc'].normal
    }
  }
})
