export default {
  backgroundColor: '#8ABEFF',
  thumbColor: '#2C2D30',
  relationStyle: {
    lineColor: '#2C2D30',
    textColor: '#2C2D30'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#2C2D30',
  summaryTextColor: '#2C2D30',
  outBorderColor: '#2C2D30',
  'fill-fill-fill': {
    0: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(69, 70, 73, 1)'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#8ABEFF'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#2C2D30'],
      strokeColors: ['#2C2D30'],
      expandColor: '#ffffff'
    }
  }
}
