export default {
  backgroundColor: '#2C2D30',
  thumbColor: '#FFFFFF',
  relationStyle: {
    lineColor: '#FFFFFF',
    textColor: '#FFFFFF'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FFFFFF',
  summaryTextColor: '#FFFFFF',
  outBorderColor: '#FFFFFF',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2C2D30'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    1: {
      colors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      textColors: ['#000000'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    normal: {
      colors: ['rgba(86, 84, 75, 1)', 'rgba(76, 87, 75, 1)', 'rgba(79, 65, 89, 1)', 'rgba(63, 74, 89, 1)'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2C2D30'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    1: {
      colors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      textColors: ['#000000'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FFFFFF'],
      textColors: ['#2C2D30'],
      strokeColors: ['#FFFFFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#FFFFFF00'],
      textColors: ['#ffffff'],
      strokeColors: ['#2C2D30'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#2C2D30'],
      textColors: ['#ffffff'],
      strokeColors: ['#ffffff'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#FFF0B8', '#CBFFB8', '#DB8FFF', '#8ABEFF'],
      expandColor: '#2C2D30',
      collapseColor: '#2C2D30'
    }
  }
}
