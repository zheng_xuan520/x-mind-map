export default {
  backgroundColor: '#3C4244',
  thumbColor: '#FDF8E7',
  relationStyle: {
    lineColor: '#FDF8E7',
    textColor: '#FDF8E7'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#FDF8E7',
  summaryTextColor: '#FDF8E7',
  outBorderColor: '#FDF8E7',
  outBorderTextColor: '#000000',
  'fill-fill-fill': {
    0: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDC9D1'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 226, 234, 1)'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDC9D1'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#FDF8E7'],
      strokeColors: ['#FDF8E7'],
      expandColor: '#ffffff'
    }
  }
}
