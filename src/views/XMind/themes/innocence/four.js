export default {
  backgroundColor: '#EA618A',
  thumbColor: '#3C4244',
  relationStyle: {
    lineColor: '#3C4244',
    textColor: '#3C4244'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#3C4244',
  summaryTextColor: '#3C4244',
  outBorderColor: '#3C4244',
  'fill-fill-fill': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(255, 255, 255, 1)'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#EA618A'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    }
  }
}
