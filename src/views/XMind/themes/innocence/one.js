export default {
  backgroundColor: '#FDF8E7',
  thumbColor: '#3C4244',
  relationStyle: {
    lineColor: '#3C4244',
    textColor: '#3C4244'
  },
  select: {
    strokeWidth: 2,
    stroke: '#2EBDFF'
  },
  summaryLineColor: '#3C4244',
  summaryTextColor: '#3C4244',
  outBorderColor: '#3C4244',
  'fill-fill-fill': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      textColors: ['#000000', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['rgba(253, 239, 227, 1)', 'rgba(249, 218, 212, 1)', 'rgba(235, 240, 235, 1)', 'rgba(218, 221, 222, 1)'],
      textColors: ['#620312', '#5A0B23', '#06345F', '#1C2B49'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    }
  },
  'fill-fill-plain': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      textColors: ['#000000', '#ffffff', '#000000', '#ffffff'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620312', '#5A0B23', '#06345F', '#1C2B49'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain': {
    0: {
      colors: ['#3C4244'],
      textColors: ['#ffffff'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620312', '#5A0B23', '#06345F', '#1C2B49'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    }
  },
  'plain-plain-plain': {
    0: {
      colors: ['#ffffff00'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620312', '#5A0B23', '#06345F', '#1C2B49'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    }
  },
  'fill-plain-plain-as-bgc': {
    0: {
      colors: ['#FDF8E7'],
      textColors: ['#3C4244'],
      strokeColors: ['#3C4244'],
      expandColor: '#ffffff'
    },
    1: {
      colors: ['#ffffff00'],
      textColors: ['#000000'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    },
    normal: {
      colors: ['#ffffff00'],
      textColors: ['#620312', '#5A0B23', '#06345F', '#1C2B49'],
      strokeColors: ['#FDC9D1', '#EA618A', '#A4D0F9', '#4F73BA'],
      expandColor: '#ffffff'
    }
  }
}
