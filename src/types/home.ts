export interface HomeConfig {
  /**
   * 页面名称
   */
  pageName: string

  /**
   * 是否显示搜索框
   */
  search: boolean

  /**
   * 是否显示添加导图按钮
   */
  add: boolean

  /**
   * 是否显示列表/卡片切换组件
   */
  grid: boolean
}

export interface HomeConfigOptions {
  recents: HomeConfig

  sharing: HomeConfig

  trash: HomeConfig

  work: HomeConfig
}
