import { createApp } from 'vue'
import FontFaceObserver from 'fontfaceobserver'
import App from './App.vue'
import router from './router'
import piniaStore from './store'
import '@/style/init.scss'
import '@/style/element.scss'
import 'remixicon/fonts/remixicon.css'

async function initApp () {
  const font1 = new FontFaceObserver('nevermind')
  const font2 = new FontFaceObserver('nevermindhand')
  const font3 = new FontFaceObserver('audiowide')
  const font4 = new FontFaceObserver('consola')
  const font5 = new FontFaceObserver('lato')
  const font6 = new FontFaceObserver('Arvo')

  Promise.all([
    font1.load('Main', 15000),
    font2.load('Main', 15000),
    font3.load('Main', 15000),
    font4.load('Main', 15000),
    font5.load('Main', 15000),
    font6.load('Main', 15000)
  ]).then(() => {
    console.log('All fonts are available')
    const app = createApp(App)
    app.use(piniaStore).use(router).mount('#app')
  })
}

initApp()
