export default [
  {
    path: '/',
    redirect: '/home/recents'
  },
  {
    path: '/home',
    name: 'Home',
    component: (): unknown => import('@/views/Home/index.vue'),
    redirect: '/home/recents',
    children: [
      {
        path: 'recents',
        name: 'Recents',
        component: (): unknown => import('@/views/Recents/index.vue')
      },
      {
        path: 'sharing',
        name: 'Sharing',
        component: (): unknown => import('@/views/Sharing/index.vue')
      },
      {
        path: 'trash',
        name: 'Trash',
        component: (): unknown => import('@/views/Trash/index.vue')
      },
      {
        path: 'work/:id?',
        name: 'Work',
        component: (): unknown => import('@/views/Work/index.vue')
      }
    ]
  },
  {
    path: '/xmind',
    name: 'XMind',
    component: (): unknown => import('@/views/XMind/index.vue')
  },
  {
    path: '/:catchAll(.*)',
    name: 'Err404',
    component: (): unknown => import('@/views/Err404.vue')
  }
]
